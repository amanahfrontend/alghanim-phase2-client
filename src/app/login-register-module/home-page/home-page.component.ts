import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from "primeng/components/common/message";
import { pagesItemSharedService } from '../../api-module/pagesItemsShared/pagesItemsShared.service';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import {DragulaModule, DragulaService} from "ng2-dragula/ng2-dragula"
import{ Subscription } from 'rxjs';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  currentUser: any;
  getDataForType: string;
  getDataForRole: string;
  items: string[];
  activeTab: string;
  getUserDataFlag: boolean;
  msg: Message[];
  itemsRoles:string[];
  role = JSON.parse(localStorage.getItem("AlghanimCurrentUser")).data.roles
  BAG = "DRAGULA_EVENTS";
  subs = new Subscription();

  constructor(private pagesItemSharedService: pagesItemSharedService,private lookupS:LookupService,private dragulaService: DragulaService) {
    this.currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));

    // this.subs.add(this.dragulaService.drag(this.BAG)
    //   .subscribe(({ el }) => {
    //     this.removeClass(el, 'ex-moved');
    //   })
    // );
    // this.subs.add(this.dragulaService.drop(this.BAG)
    //   .subscribe(({ el }) => {
    //     this.addClass(el, 'ex-moved');
    //   })
    // );
    // this.subs.add(this.dragulaService.over(this.BAG)
    //   .subscribe(({ el, container }) => {
    //     console.log('over', container);
    //     this.addClass(container, 'ex-over');
    //   })
    // );
    // this.subs.add(this.dragulaService.out(this.BAG)
    //   .subscribe(({ el, container }) => {
    //     console.log('out', container);
    //     this.removeClass(container, 'ex-over');
    //   })
    // );

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addClass() {}
  removeClass() {}

  ngOnInit() {
    this.pagesItemSharedService.getItemsValues();

    this.items = ['Area', 'Attendance Status', 'Availabilty', 'Building Types', 'Company Code', 'Contract Type', 'Cost Center', 'Division', 'Governorates', 'Shift', 'Languages'];
    // this.itemsRoles = ['Admin', 'Dispatchers', 'Supervisor', 'Engineers', 'Drivers', 'Foremen', 'Technicians'];
    this.getRoles();
    this.getUserDataFlag = false;
    if(this.role == 'Dispatcher'){
      this.activeTab = 'orderManagement';
    }
    else{
      this.activeTab = 'data';
    }
    this.getData('Area');
    this.getUserData('Supervisor');
  }

  getRoles(){
    this.lookupS.getAllRoles().subscribe(result =>{
      this.itemsRoles = result.data
    },error =>{
      console.log('error')
    })
  }

  setActiveTab(tabName) {
    this.activeTab = tabName;
    if (tabName == 'usersManagement') {
      this.getUserDataFlag = false;
    } else if (tabName == 'teamManagement') {
      this.getUserDataFlag = false;
    } else if (tabName == 'orderManagement') {
      this.getUserDataFlag = false;
    } else if (tabName == 'dispatcherSetting') {
      this.getUserDataFlag = false;
    } else if (tabName == 'data') {
      this.getUserDataFlag = false;
    }
  }

  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }
  applyActiveTab(role) {
    if (this.itemsRoles.indexOf(role) == 0) {
      return 'uk-active'
    }
  }

  getData(type) {
    this.getDataForType = type;
    console.log(this.getDataForType);
  }

  getUserData(role) {
    this.getDataForRole = role;
    console.log(this.getDataForRole);
  }


}