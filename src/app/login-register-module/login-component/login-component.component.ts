import { AuthenticationServicesService } from './../../api-module/services/authentication/authentication-services.service';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import {HubConnection} from "@aspnet/signalr-client";
// import {quotationHubUrl} from "../../api-module/services/globalPath";
import { pagesItemSharedService } from '../../api-module/pagesItemsShared/pagesItemsShared.service'


@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;
  arr: any = [];

  // private _hubConnection: HubConnection;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationServicesService,
    private alertService: AlertServiceService,
    private pagesItemSharedService: pagesItemSharedService) {
  }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    console.log(this.model.username)
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          console.log(data);
          console.log(this.returnUrl);
          if (data.data.roles) {
            if (data.data.roles[0] == 'Dispatcher') {
              this.arr = ["orderManagement"];
              this.pagesItemSharedService.getItemsValues();
              for (let i = 0; i < this.arr.length; i++) {
                this.pagesItemSharedService.actionHideMe[this.arr[i]] = true;
              }
              localStorage.setItem('hideMe', JSON.stringify(this.pagesItemSharedService.actionHideMe));
            }
            else {
              this.arr = ["masterData", "usersManagement", "teamManagement", "dispatcherSetting"];
              this.pagesItemSharedService.getItemsValues();
              for (let i = 0; i < this.arr.length; i++) {
                this.pagesItemSharedService.actionHideMe[this.arr[i]] = true;
              }
              localStorage.setItem('hideMe', JSON.stringify(this.pagesItemSharedService.actionHideMe));
            }
          }
          this.router.navigate(['./' + this.returnUrl]);

          // this.authenticationService.activateSignalR();
          //   this.pagesItemSharedService.setItemsValues(arr);
          // if (data.roles[0] == 'Admin') {
          //   this.router.navigate(['/admin']);
          // }


          // this.router.navigate(['./' + this.returnUrl]);
          // this.authenticationService.activateSignalR();
        },
        error => {
          //console.log(error);
          this.alertService.error('Username or password is incorrect');
          this.loading = false;
        });
  }

}
