import { FormsModule } from '@angular/forms';
import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { RouterModule, Routes } from '@angular/router';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { LoginComponentComponent } from './../login-register-module/login-component/login-component.component';
import { HomePageComponent } from './../login-register-module/home-page/home-page.component';
// import { AdminModule } from '../admin/admin.module'
import { AdminMainPageComponent } from '../admin/admin-main-page/admin-main-page.component';
import {userManagmentComponent} from '../admin/userManagment/userManagment.component';
import {SimpleAdminContentComponent} from '../admin/simple-admin-content/simple-admin-content.component';
import {AdminUsersComponent} from '../admin/admin-users/admin-users.component';
import {AdminUploadResourcesComponent} from '../admin/admin-upload-resources/admin-upload-resources.component';
import {AssignTecComponent} from '../admin/assign-tec/assign-tec.component';
import {SettingDispatchersComponent} from '../admin/setting-dispatchers/setting-dispatchers.component';
import {EditDisAndProbModalComponent} from '../admin/setting-dispatchers/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component';
import {teamManagmentComponent} from '../admin/teamManagement/teamManagement.component';
import {orderManagmentComponent} from '../admin/orderManagement/orderManagement.component';


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';


const routes: Routes = [
  { path: '', component: HomePageComponent, canActivate: [AuthGuardGuard],data: { roles: ['CallCenter', 'Maintenance', 'Admin', 'Dispatcher'] }},
  // { path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
  { path: 'login', component: LoginComponentComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModuleModule,
    RouterModule.forChild(routes),
    DragulaModule,
  ],
  declarations: [
    LoginComponentComponent,
    HomePageComponent,teamManagmentComponent,
    AdminMainPageComponent,userManagmentComponent,
    SimpleAdminContentComponent, AdminUsersComponent, 
    AdminUploadResourcesComponent, AssignTecComponent, 
    SettingDispatchersComponent, EditDisAndProbModalComponent,
    orderManagmentComponent
  ],
  entryComponents: [
    EditDisAndProbModalComponent
  ]
})
export class LoginRegisterModuleModule { }
