import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";


@Component({
    selector: 'app-teamManagement',
    templateUrl: './teamManagement.component.html',
    styleUrls: ['./teamManagement.component.css']
})
export class teamManagmentComponent implements OnInit {

    constructor(private lookUp: LookupService, private messageService: MessageService, private modalService: NgbModal, private utilities: UtilitiesService) {
    }

    unassignedMembers: any = [];
    dispatchers: any = []

    ngOnInit() {
        this.getUnassignedMempers();
        this.getDispatchers();

    }

    getUnassignedMempers() {
        this.lookUp.getAllUnassignedMempers().subscribe(result => {
            this.unassignedMembers = result.data;

        })
    }

    getDispatchers() {
        this.lookUp.getAllDispatchers().subscribe(result => {
            this.dispatchers = result.data;
            console.log(this.dispatchers)

        })
    }


}