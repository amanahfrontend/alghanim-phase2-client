// import {allCustomerLookup} from './../../models/lookups-modal';
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import * as myGlobals from '../globalPath';
import {UtilitiesService} from "../utilities/utilities.service";
import { retry } from 'rxjs/operators';

@Injectable()
export class LookupService {

  constructor(private http: Http, private utilities: UtilitiesService) {
  }

  /*new services Mohamed Ragab*/

  /*GET*/

  getAllPreventive() {
    return this.http.get(myGlobals.allPreventive, this.jwt()).map((response: Response) => response.json());
  }

  getAllAvailability() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.availabilitysUrl, this.jwt()).map((response: Response) => response.json());
  }

  getBuildingTypes() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesUrl, this.jwt()).map((response: Response) => response.json());
  }

  getAllAreasDataManagement() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.areasUrl, this.jwt()).map((response: Response) => response.json());
  }

  getAllAreasDataManagementPaginated(pageNoSize) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.areasUrl,pageNoSize, this.jwt()).map((response: Response) => response.json());
  }

  getCallsHistory() {
    return this.http.get(myGlobals.callsHistory, this.jwt()).map((response: Response) => response.json())
  }

  getAllAttendanceStates() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.attendanceStatusUrl, this.jwt()).map((response: Response) => response.json());
  }

  getCallLogs(id) {
    return this.http.get(myGlobals.callsLogs + id, this.jwt()).map((response: Response) => response.json());
  }

  getCustomerCall(id) {
    return this.http.get(myGlobals.GetCustomerDetailsByID + id, this.jwt()).map((response: Response) => response.json());
  }

  getCompanyCodes() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.companyCodeUrl, this.jwt()).map((response: Response) => response.json());
  }

  getItem(code) {
    return this.http.get(myGlobals.item + code, this.jwt()).map((response: Response) => response.json());
  }

  getAllEstimations() {
    return this.http.get(myGlobals.estimations, this.jwt()).map((response: Response) => response.json());
  }

  searchByNameOrPhone(searchPhrase) {
    return this.http.get(myGlobals.searchByPhoneOrName + searchPhrase, this.jwt()).map((response: Response) => response.json());
  }

  getEstimationDetails(id) {
    return this.http.get(myGlobals.estimationDetails + id, this.jwt()).map((response: Response) => response.json());
  }

  getAllQuotations() {
    return this.http.get(myGlobals.quotations, this.jwt()).map((response: Response) => response.json());
  }

  getQuoutationByRefernceNo(searchText) {
    return this.http.get(myGlobals.searchQuotation + searchText, this.jwt()).map((response: Response) => response.json());
  }

  getQuotationDetailsById(id) {
    return this.http.get(myGlobals.quotationbyId + id, this.jwt()).map((response: Response) => response.json());
  }

  getQuotationDetailsByRefNo(refNo) {
    return this.http.get(myGlobals.quotationbyRefNo + refNo, this.jwt()).map((response: Response) => response.json());
  }

  getContractTypes() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.contractTypesUrl, this.jwt()).map((response: Response) => response.json());
  }

  searchContract(refNo) {
    return this.http.get(myGlobals.searchContract + refNo, this.jwt()).map((response: Response) => response.json());
  }

  getAllContracts() {
    return this.http.get(myGlobals.allContracts, this.jwt()).map((response: Response) => response.json());
  }

  getContractById(id) {
    return this.http.get(myGlobals.contractById + id, this.jwt()).map((response: Response) => response.json());
  }

  getRoles() {
    return this.http.get(myGlobals.roles, this.jwt()).map((response: Response) => response.json());
  }

  getCostCenter() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.costCenterUrl, this.jwt()).map((response: Response) => response.json());
  }

  getUsers() {
    return this.http.get(myGlobals.users, this.jwt()).map((response: Response) => response.json());
  }

  getDivision() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.divisionUrl, this.jwt()).map((response: Response) => response.json());
  }

  getGovernorates() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.governoratesUrl, this.jwt()).map((response: Response) => response.json());
  }
  
  getShift() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.shiftUrl, this.jwt()).map((response: Response) => response.json());
  }

  getlanguages() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.languagesUrl, this.jwt()).map((response: Response) => response.json());
  }

  getOrderPriority() {
    return this.http.get(myGlobals.orderPriorities, this.jwt()).map((response: Response) => response.json());
  }

  getAllOrders() {
    return this.http.get(myGlobals.allOrders, this.jwt()).map((response: Response) => response.json());
  }

  searchByOrderNumber(number) {
    return this.http.get(myGlobals.searchOrders + number, this.jwt()).map((response: Response) => response.json());
  }

  getOrderStatus() {
    return this.http.get(myGlobals.orderStatus, this.jwt()).map((response: Response) => response.json());
  }

  getContractByCustomerId(id) {
    return this.http.get(myGlobals.contractByCustomer + id, this.jwt()).map((response: Response) => response.json());
  }

  getOrderByCustomerId(id) {
    return this.http.get(myGlobals.orderByCustomer + id, this.jwt()).map((response: Response) => response.json());
  }

  getCallByCustomerId(id) {
    return this.http.get(myGlobals.callrByCustomer + id, this.jwt()).map((response: Response) => response.json());
  }

  getLocationByCustomerId(id) {
    return this.http.get(myGlobals.locationByCustomer + id, this.jwt()).map((response: Response) => response.json());
  }

  getDispatcherOrders(id) {
    return this.http.get(myGlobals.ordersByDis + id, this.jwt()).map((response: Response) => response.json());
  }

  getDispatchers() {
    return this.http.get(myGlobals.allDispatchers, this.jwt()).map((response: Response) => response.json());
  }

  getTecs() {
    return this.http.get(myGlobals.unAssignedTecs, this.jwt()).map((response: Response) => response.json());
  }

  getAllTecs() {
    return this.http.get(myGlobals.allTecs, this.jwt()).map((response: Response) => response.json());
  }

  getTecsByDis(id) {
    return this.http.get(myGlobals.assignedTecs + id, this.jwt()).map((response: Response) => response.json());
  }

  getOrderProgress(id) {
    return this.http.get(myGlobals.orderProgressUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  getProgressStatuses() {
    return this.http.get(myGlobals.progressStatuses, this.jwt()).map((response: Response) => response.json());
  }

  getDispatchersWithProb() {
    return this.http.get(myGlobals.getDiByProb, this.jwt()).map((response: Response) => response.json());
  }

  getAllProblems() {
    return this.http.get(myGlobals.allProbs, this.jwt()).map((response: Response) => response.json());
  }

  getDispatcherAllProblems(id) {
    return this.http.get(myGlobals.getDisWithAllProbs + id, this.jwt()).map((response: Response) => response.json());
  }

  getAllVehicles() {
    return this.http.get(myGlobals.getAllVehicles, this.jwt()).map((response: Response) => response.json());
  }

  getDisAllOrders(id) {
    return this.http.get(myGlobals.getOrdersByDis + id, this.jwt()).map((response: Response) => response.json());
  }

  getTecAssignedItems(id) {
    return this.http.get(myGlobals.assignedItems + id, this.jwt()).map((response: Response) => response.json());
  }

  getTecReportUsedItems(id) {
    return this.http.get(myGlobals.usedItems + id, this.jwt()).map((response: Response) => response.json());
  }

  getAllCustomers() {
    return this.http.get(myGlobals.allCustomers, this.jwt()).map((response: Response) => response.json());
  }

  getAllItems() {
    return this.http.get(myGlobals.getAllItems, this.jwt()).map((response: Response) => response.json());
  }

  downloadQuotationWord(id) {
    return this.http.get(myGlobals.quotationWordUrl + id, this.jwt()).map((response: Response) => response);
  }

  getAllOrderProgress() {
    return this.http.get(myGlobals.allOrderProgressUrl, this.jwt()).map((response: Response) => response.json());
  }

  getAllOrderProblems() {
    return this.http.get(myGlobals.allOrderProblemsUrl , this.jwt()).map((response: Response) => response.json());
  }

  getCustomerDetails(customerId) {
    return this.http.get(myGlobals.getCustomerDetails + customerId, this.jwt()).map((response: Response) => response.json());
  }

  // downloadQuotationWord(id) {
  //   console.log(myGlobals.quotationWordUrl + id);
  //   return this.http.get(myGlobals.quotationWordUrl + id, this.jwt()).map((response: Response) => response.json());
  // }

  /*POST*/

  postNewLog(newLog) {
    return this.http.post(myGlobals.logPostUrl, newLog, this.jwt()).map((response: Response) => response.json());
  }

  postEstimation(estimations) {
    return this.http.post(myGlobals.estimationPostUrl, estimations, this.jwt()).map((response: Response) => response.json());
  }

  postNewCall(call) {
    return this.http.post(myGlobals.callPostUrl, call, this.jwt()).map((response: Response) => response.json());
  }

  generateQuotation(estimation) {
    return this.http.post(myGlobals.generateQuotationUrl, estimation, this.jwt()).map((response: Response) => response.json());
  }

  postNewContract(newContract) {
    return this.http.post(myGlobals.generateContractUrl, newContract, this.jwt()).map((response: Response) => response.json());
  }

  postNewUser(newUser) {
    return this.http.post(myGlobals.newUserUrl, newUser, this.jwt()).map((response: Response) => response.json());
  }

  postItemsExcelSheet(excelSheetObject) {
    return this.http.post(myGlobals.itemsExcelSheetUrl, excelSheetObject, this.jwt()).map((response: Response) => response.json());
  }

  postSalaryExcelSheet(excelSheetObject) {
    return this.http.post(myGlobals.salaryExcelSheetUrl, excelSheetObject, this.jwt()).map((response: Response) => response.json());
  }

  postNewOrder(newOrder) {
    return this.http.post(myGlobals.postNewOrderUrl, newOrder, this.jwt()).map((response: Response) => response.json());
  }

  postCustomerTypes(customerType) {
    return this.http.post(myGlobals.postCustomerTypesUrl, customerType, this.jwt()).map((response: Response) => response.json());
  }

  postCallsTypes(callType) {
    return this.http.post(myGlobals.postCallsTypesUrl, callType, this.jwt()).map((response: Response) => response.json());
  }

  postCallPriorities(callPriority) {
    return this.http.post(myGlobals.postCallPrioritiesUrl, callPriority, this.jwt()).map((response: Response) => response.json());
  }

  postPhoneTypes(phoneType) {
    return this.http.post(myGlobals.postPhoneTypesUrl, phoneType, this.jwt()).map((response: Response) => response.json());
  }

  postActionStatues(status) {
    return this.http.post(myGlobals.postActionStateUrl, status, this.jwt()).map((response: Response) => response.json());
  }

  postContractTypes(contractType) {
    return this.http.post(myGlobals.postContractTypeUrl, contractType, this.jwt()).map((response: Response) => response.json());
  }

  postRoles(role) {
    return this.http.post(myGlobals.postRoleUrl, role, this.jwt()).map((response: Response) => response.json());
  }

  postOrderType(orderType) {
    return this.http.post(myGlobals.postOrderTypeUrl, orderType, this.jwt()).map((response: Response) => response.json());
  }

  postOrderPriority(orderPriority) {
    return this.http.post(myGlobals.postOrderPriorityUrl, orderPriority, this.jwt()).map((response: Response) => response.json());
  }

  postOrderStatus(orderState) {
    return this.http.post(myGlobals.postOrderStateUrl, orderState, this.jwt()).map((response: Response) => response.json());
  }

  postTecnichanWithDis(tecWithDis) {
    return this.http.post(myGlobals.postTecForDist, tecWithDis, this.jwt()).map((response: Response) => response.json());
  }

  postOrderToTec(orderByTec) {
    return this.http.post(myGlobals.orderByTec, orderByTec, this.jwt()).map((response: Response) => response.json());
  }

  postUnAssignedOrder(id) {
    return this.http.get(myGlobals.unAssignedOrder + id, this.jwt()).map((response: Response) => response.json());
  }

  postTecAvailability(availability) {
    return this.http.post(myGlobals.tecAvailable, availability, this.jwt()).map((response: Response) => response.json());
  }

  postVisitTime(date) {
    return this.http.post(myGlobals.postPreferedVisitDate, date, this.jwt()).map((response: Response) => response.json());
  }

  postTransferOrder(transferToDispatcher) {
    return this.http.post(myGlobals.transferToDispatcherUrl, transferToDispatcher, this.jwt()).map((response: Response) => response.json());
  }

  postProgress(progress) {
    return this.http.post(myGlobals.postProgressUrl, progress, this.jwt()).map((response: Response) => response.json());
  }

  postDispatcherWithProblemsAndAreas(DisWithProb) {
    console.log('postDispatcherWithProblemsAndAreas',DisWithProb);
    return this.http.post(myGlobals.postDispatcherWithProblemsUrl, DisWithProb, this.jwt()).map((response: Response) => response.json());
  }

  filterOrders(filterObj) {
    return this.http.post(myGlobals.filterOrdersUrl, filterObj, this.jwt()).map((response: Response) => response.json());
  }

  filterOrdersMap(filterObj) {
    return this.http.post(myGlobals.filterOrdersMapUrl, filterObj, this.jwt()).map((response: Response) => response.json());
  }

  assignItemToTec(obj) {
    return this.http.post(myGlobals.assignItemUrl, obj, this.jwt()).map((response: Response) => response.json());
  }

  postCustomer(obj) {
    return this.http.post(myGlobals.postCustomerUrl, obj, this.jwt()).map((response: Response) => response.json());
  }

  assignOrdersToDispatcher(obj) {
    return this.http.post(myGlobals.assignOrdersToDispatcherUrl, obj, this.jwt()).map((response: Response) => response.json());
  }

  assignOrdersToTec(obj) {
    return this.http.post(myGlobals.assignOrdersToTecUrl, obj, this.jwt()).map((response: Response) => response.json());
  }

  searchAssignedItems(searchObject) {
    return this.http.post(myGlobals.assignedItemSearch, searchObject, this.jwt()).map((response: Response) => response.json());
  }

  postOrderProgress(objectProgress) {
    return this.http.post(myGlobals.postOrderProgress, objectProgress, this.jwt()).map((response: Response) => response.json());
  }

  postOrderProblems(objectProblems) {
    return this.http.post(myGlobals.postOrderProblems, objectProblems, this.jwt()).map((response: Response) => response.json());
  }

  filterUsedItems(filterObject) {
    return this.http.post(myGlobals.filterUsedItems, filterObject, this.jwt()).map((response: Response) => response.json());
  }

  filterPreventive(filterObject) {
    return this.http.post(myGlobals.filterPreventiveUrl, filterObject, this.jwt()).map((response: Response) => response.json());
  }

  filterEstimations(filterObject) {
    return this.http.post(myGlobals.filterEstimationsUrl, filterObject, this.jwt()).map((response: Response) => response.json());
  }

  filterQuotations(filterObject) {
    return this.http.post(myGlobals.filterQuotationsUrl, filterObject, this.jwt()).map((response: Response) => response.json());
  }

  postNewLocation(location) {
    return this.http.post(myGlobals.postNewCustomerLocationUrl, location, this.jwt()).map((response: Response) => response.json());
  }

  ////////////////////////////////////////// New //////////////////////////////////////////////////
  getAllRoles(){
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.rolesUrl,this.jwt()).map((response: Response) => response.json());
  }
  postArea(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAreasUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  editArea(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAreasUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getAreaLangById(areaId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.areaLanguageByIdUrl + areaId ,this.jwt()).map((response: Response) => response.json());
  }
  deleteArea(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAreaUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  postAttendanceStates(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAttendanceStates, obj, this.jwt()).map((response: Response) => response.json());
  }
  editAttendanceStates(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAttendanceStatesUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteAttendanceStates(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAttendanceStatesUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getAttendanceStatesById(attendId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.attendanceStatesByIdUrl + attendId ,this.jwt()).map((response: Response) => response.json());
  }
  postAvailability(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAvailability, obj, this.jwt()).map((response: Response) => response.json());
  }
  editAvailability(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAvailabilityUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteAvailability(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAvailabilityUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getAvailabilityById(avaliblId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.availabilityByIdUrl + avaliblId ,this.jwt()).map((response: Response) => response.json());
  }
  postBuildingTypes(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postBuildingTypes, obj, this.jwt()).map((response: Response) => response.json());
  }
  editBuildingTypes(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putBuildingTypesUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteBuildingTypes(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteBuildingTypesUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getBuildingTypesById(buildId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesByIdUrl + buildId ,this.jwt()).map((response: Response) => response.json());
  }
  postCompanyCode(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCompanyCode, obj, this.jwt()).map((response: Response) => response.json());
  }
  editCompanyCode(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putCompanyCodeUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteCompanyCode(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteCompanyCodeUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getCompanyCodeById(companyId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.companyCodeByIdUrl + companyId ,this.jwt()).map((response: Response) => response.json());
  }
  postContractType(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postContractTypes, obj, this.jwt()).map((response: Response) => response.json());
  }
  editContractTypes(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putContractTypesUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteContractTypes(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteContractTypesUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getContractTypeById(contId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.contractTypesByIdUrl + contId ,this.jwt()).map((response: Response) => response.json());
  }
  postCostCenter(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCostCenter, obj, this.jwt()).map((response: Response) => response.json());
  }
  editCostCenter(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putCostCenterUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteCostCenter(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteCostCenterUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getCostCenterById(costId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.CostCentersByIdUrl + costId ,this.jwt()).map((response: Response) => response.json());
  }
  postDivision(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postDivision, obj, this.jwt()).map((response: Response) => response.json());
  }
  editDivision(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putDivisionUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteDivision(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteDivisionUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getDivisionById(DivisionId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.divisionByIdUrl + DivisionId ,this.jwt()).map((response: Response) => response.json());
  }
  postGovernorates(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postGovernorates, obj, this.jwt()).map((response: Response) => response.json());
  }
  editGovernorates(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putGovernoratesUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteGovernorates(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteGovernoratesUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getGovernoratesById(govId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.governoratesByIdUrl + govId ,this.jwt()).map((response: Response) => response.json());
  }
  postShift(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postShift, obj, this.jwt()).map((response: Response) => response.json());
  }
  editShift(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putShiftUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteShift(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteShiftUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  getShiftById(govId){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.ShiftByIdUrl + govId ,this.jwt()).map((response: Response) => response.json());
  }
  postLanguages(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postLanguages, obj, this.jwt()).map((response: Response) => response.json());
  }
  editLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putLanguagesUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteLanguages(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteLanguagesUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  postAreaLanguages(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAreaLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editAreaLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAreaLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteAreaLanguages(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAreaLanguagesUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  postAttendanceStatesLanguages(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAttendanceStatesLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editAttendanceStatesLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAttendanceStatesLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  deleteAttendanceStatesLanguages(id){
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAttendanceStatesLanguagesUrl + id, this.jwt()).map((response: Response) => response.json());
  }
  postAvailabilityLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAvailabilityLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editAvailabilityLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAvailabilityLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postBuildingTypesLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postBuildingTypesLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editBuildingTypesLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putBuildingTypesLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postCompanyCodeLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCompanyCodeLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editCompanyCodeLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putCompanyCodeLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postContractTypesLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postContractTypesLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editContractTypesLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putContractTypesLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postCostCenterLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCostCenterLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editCostCenterLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putLang_CostCenterLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postDivisionLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postDivisionLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editDivisionLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putDivisionLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postGovernoratesLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postGovernoratesLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editGovernoratesLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putGovernoratesLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  postShiftLanguage(obj){
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postShiftLanguage, obj, this.jwt()).map((response: Response) => response.json());
  }
  editShiftLanguages(obj){
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putShiftLanguageUrl, obj, this.jwt()).map((response: Response) => response.json());
  }

  ///////////User Management//////////
  getAllUserAmin(){
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.adminUserUrl, this.jwt()).map((response: Response) => response.json());
  }
  getUserById(id){
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.getrUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  postUser(obj){
    return this.http.post(myGlobals.BaseUrlIdentity + myGlobals.postUserUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  editUser(obj){
    return this.http.put(myGlobals.BaseUrlIdentity + myGlobals.putUserUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getAllDispatchers(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherUrl, this.jwt()).map((response: Response) => response.json());
  }
  getDispatcherById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  getDispatcherBySuper(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatchersUserBySuper + id, this.jwt()).map((response: Response) => response.json());
  }
  postUserDispatcher(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postUserDispatcherUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  editUserDispatcher(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.putUserDispatcherUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getAllDrivers(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDriverUrl, this.jwt()).map((response: Response) => response.json());
  }
  getAllSuperVisor(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getSuperVisorUrl, this.jwt()).map((response: Response) => response.json());
  }
  postSuperVisor(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postSupervisorUrl, obj, this.jwt()).map((response: Response) => response.json()); 
  }
  editUserSuperVisor(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.putUserSupervisorUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getSuperVisorById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getSupervisorUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  getSuperVisorByDivision(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getSupervisorUserBydivison + id, this.jwt()).map((response: Response) => response.json());
  }
  postDriver(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postDriverUrl, obj, this.jwt()).map((response: Response) => response.json()); 
  }
  editUserDiver(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.putUserDriverUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getDriverById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDriverUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  getDriverByDivision(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDriverUserBydivison + id, this.jwt()).map((response: Response) => response.json());
  }
  getAllEngineers(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getEngineerUrl, this.jwt()).map((response: Response) => response.json());
  }
  postEngineer(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postEngineerUrl, obj, this.jwt()).map((response: Response) => response.json()); 
  }
  editUserEngineer(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.putUserEngineerUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getEngineerById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getEngineerUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  getEngineerByDivision(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getEngineerUserBydivison + id, this.jwt()).map((response: Response) => response.json());
  }
  getAllTechnicians(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getTechnicianUrl, this.jwt()).map((response: Response) => response.json());
  }
  postTechnician(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postTechnicianUrl, obj, this.jwt()).map((response: Response) => response.json()); 
  }
  editUserTechnician(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.putUserTechnicianUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getTechnicianById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getTechnicianUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  getAllForemans(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getForemanUrl, this.jwt()).map((response: Response) => response.json());
  }
  postForeman(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postForemanUrl, obj, this.jwt()).map((response: Response) => response.json()); 
  }
  editUserForeman(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.putUserForemanUrl, obj, this.jwt()).map((response: Response) => response.json());
  }
  getForemanById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getForemanUserById + id, this.jwt()).map((response: Response) => response.json());
  }
  getForemanByDispatcher(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getFormanUserByDispatcher + id, this.jwt()).map((response: Response) => response.json());
  }
  getAllMangaer(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getManagerUrl , this.jwt()).map((response :Response) => response.json());
  }
  getManagerById(id){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.getManagerById + id ,this.jwt()).map((response: Response) => response.json());
  }
  editUserManager(obj){
  return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.PutUserManagerUrl , obj ,this.jwt()).map((response: Response) => response.json());
  }
  postManager(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postManagerUrl , obj ,this.jwt()).map((response :Response) => response.json());
  }

  getAllMaterial(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getMaterialUrl ,this.jwt()).map((response :Response) => response.json());
  }
  getMatrialById(id){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.getControllerById + id , this.jwt()).map((response :Response) => response.json());
  }
  editUserControl(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.PutUserMatrialUrl , obj , this.jwt()).map((response :Response) => response.json());
  }
  postMatrialcontroller(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postMatrialController ,obj , this.jwt()).map((response: Response) => response.json());
  }



  ///////////Team Management//////////
  getAllUnassignedMempers(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getUnassignedMempersUrl, this.jwt()).map((response: Response) => response.json());
  }

  getTeamByDispatcherById(id){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getByDispatcherByIdUrl  + id, this.jwt()).map((response: Response) => response.json());
  }

  getTeams(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getTeams , this.jwt()).map((response: Response) => response.json());
  }

  postTeam(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postTeam, obj, this.jwt()).map((response: Response) => response.json()); 
  }

  assginMemberToTeam(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.assignTeamMember + '?teamId=' + obj.teamId + '&memberId=' + obj.memberId,null, this.jwt()).map((response: Response) => response.json());
  }
  reassignTeamToDispatcher(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.reassignTeamToDispatcher + '?teamId=' + obj.teamId + '&dispatcherId=' + obj.dispatcherId,null, this.jwt()).map((response: Response) => response.json());
  }
  unAssginMember(obj){
    return this.http.put(myGlobals.BaseUrlUserManagementNew + myGlobals.unassignedMember + '?memberId=' + obj.memberId,null, this.jwt()).map((response: Response) => response.json());
  }


  ///////////dispatcherSetting////////////
  getAllOrderProblem(){
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.OrderProblem , this.jwt()).map((response: Response) => response.json());
  }
  getAllDispatcherSettings(){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.disSettingUrl , this.jwt()).map((response: Response) => response.json());
  }
  getDispatcherSettingById(disId){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherSettingById.replace('{0}', disId) , this.jwt()).map((response: Response) => response.json());
  }
  deleteDisDetting(disId){
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.deleteDispatcherSetting.replace('{0}', disId) , this.jwt()).map((response: Response) => response.json());
  }
  postProblemArea(obj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postProblemArea, (obj) , this.jwt()).map((response: Response) => response.json());
  }
  postDispatcherAllProblemsandArea(Disobj){
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.postDispatcherAllProblemsandArea , Disobj , this.jwt()).map((response: Response) => response.json())
  }




  //////////////////order management////////////////////////////
  getAllUnAssignedOrdersForDispatcher(dispatcherId){
    return this.http.get(myGlobals.BaseUrlOrderManagementNew + myGlobals.unAssignedOrdersForDispatcherUrl + dispatcherId, this.jwt()).map((response: Response) => response.json());
  }
  getAllAssignedOrdersForDispatcher(dispatcherId){
    return this.http.get(myGlobals.BaseUrlOrderManagementNew + myGlobals.assignedOrdersForDispatcherUrl + dispatcherId, this.jwt()).map((response: Response) => response.json());
  }
  assginOrder(obj){
    return this.http.put(myGlobals.BaseUrlOrderManagementNew + myGlobals.assigndOrdersUrl,obj, this.jwt()).map((response: Response) => response.json());
  }
  unAssginOrder(obj){
    return this.http.put(myGlobals.BaseUrlOrderManagementNew + myGlobals.unAssigndOrdersUrl,obj, this.jwt()).map((response: Response) => response.json());
  }
 


  /*DELETE*/

  deleteEstimation(id) {
    return this.http.delete(myGlobals.deletEstimationUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteQuotation(id) {
    return this.http.delete(myGlobals.deleteQuotationUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteContract(id) {
    return this.http.delete(myGlobals.deleteContractUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteCustomertype(id) {
    return this.http.delete(myGlobals.deleteCustomertypeUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteCallType(id) {
    return this.http.delete(myGlobals.deleteCallTypeUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deletePriorities(id) {
    return this.http.delete(myGlobals.deletePrioritiesUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deletePhoneTypes(id) {
    return this.http.delete(myGlobals.deletePhoneTypeUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteStatus(id) {
    return this.http.delete(myGlobals.deleteStatusUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteContractType(id) {
    return this.http.delete(myGlobals.deleteContractTypeUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteRole(id) {
    return this.http.delete(myGlobals.deleteRoleUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteUser(id) {
    return this.http.delete(myGlobals.deleteUserUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteOrder(id) {
    return this.http.delete(myGlobals.deleteOrderUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteOrderType(id) {
    return this.http.delete(myGlobals.deleteOrderTypeUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteOrderPriority(id) {
    return this.http.delete(myGlobals.deleteOrderPriorityUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  deleteOrderStatus(id) {
    return this.http.delete(myGlobals.deleteOrderStatusUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  removeTecFromDis(id) {
    return this.http.delete(myGlobals.deleteTecFromDis + id, this.jwt()).map((response: Response) => response.json());
  }

  removeDisByProb(id) {
    return this.http.delete(myGlobals.removeDisWithProb + id, this.jwt()).map((response: Response) => response.json());
  }

  removeProgressStatus(id) {
    return this.http.delete(myGlobals.removeProgressStatusUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  removeProblem(id) {
    return this.http.delete(myGlobals.removeProblemUrl + id, this.jwt()).map((response: Response) => response.json());
  }

  /*UPDATE*/

  updateEstimation(estimation) {
    return this.http.put(myGlobals.updateEstimationUrl, estimation, this.jwt()).map((response: Response) => response.json());
  }

  updateCustomerTypes(customerType) {
    return this.http.put(myGlobals.updateCustomerTypeUrl, customerType, this.jwt()).map((response: Response) => response.json());
  }

  updateCallsTypes(callType) {
    return this.http.put(myGlobals.updateCallTypeUrl, callType, this.jwt()).map((response: Response) => response.json());
  }

  updatePriorities(priority) {
    return this.http.put(myGlobals.updatePriorityUrl, priority, this.jwt()).map((response: Response) => response.json());
  }

  updatePhoneTypes(phoneType) {
    return this.http.put(myGlobals.updatePhoneTypeUrl, phoneType, this.jwt()).map((response: Response) => response.json());
  }

  updateStatues(status) {
    return this.http.put(myGlobals.updateStatusUrl, status, this.jwt()).map((response: Response) => response.json());
  }

  updateContractTypes(contractType) {
    return this.http.put(myGlobals.updateContractTypeUrl, contractType, this.jwt()).map((response: Response) => response.json());
  }

  updateRoles(role) {
    return this.http.post(myGlobals.updateRoleUrl, role, this.jwt()).map((response: Response) => response.json());
  }

  updateUser(userData) {
    return this.http.post(myGlobals.updateUserUrl, userData, this.jwt()).map((response: Response) => response.json());
  }

  updateOrder(order) {
    return this.http.put(myGlobals.updateOrderUrl, order, this.jwt()).map((response: Response) => response.json());
  }

  updateOrderStatus(status) {
    return this.http.put(myGlobals.updateOrderStatusUrl, status, this.jwt()).map((response: Response) => response.json());
  }

  updateOrderPriority(priority) {
    return this.http.put(myGlobals.updateOrderPriorityUrl, priority, this.jwt()).map((response: Response) => response.json());
  }

  updateOrderType(type) {
    return this.http.put(myGlobals.updateOrderTypeUrl, type, this.jwt()).map((response: Response) => response.json());
  }

  updateOrderProgressLookUp(progress) {
    return this.http.put(myGlobals.updateOrderProgressUrl, progress, this.jwt()).map((response: Response) => response.json());
  }

  updateOrderProblemsLookUp(problem) {
    return this.http.put(myGlobals.updateOrderProblemsUrl, problem, this.jwt()).map((response: Response) => response.json());
  }

  updateCustomer(customerData) {
    return this.http.put(myGlobals.updateCustomerUrl, customerData, this.jwt()).map((response: Response) => response.json());
  }

  updateLocation(location) {
    return this.http.post(myGlobals.updateLocationUrl, location, this.jwt()).map((response: Response) => response.json());
  }

  /*-----------------------------------------------*/

  GetallGovernorates() {
    return this.http.get(myGlobals.allGovernorates, this.jwt()).map((response: Response) => response.json());

  }

  GetallAreas(Gov: number) {
    return this.http.get(myGlobals.allAreas + '?govId=' + Gov, this.jwt()).map((response: Response) => response.json());

  }

  GetallBlocks(area: number) {
    return this.http.get(myGlobals.allBlocks + '?areaId=' + area, this.jwt()).map((response: Response) => response.json());

  }

  GetallStreets(area: number, block: string, gov: number) {
    return this.http.get(myGlobals.allStreets + '?govId=' + gov + '&areaId=' + area + '&blockName=' + block, this.jwt()).map((response: Response) => response.json());

  }

  GetLocationByPaci(PACI: number) {
    return this.http.get(myGlobals.allGetLocationByPaci + PACI, this.jwt()).map((response: Response) => response.json());

  }

// private helper methods
  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
    if (currentUser && currentUser.data.token) {
      let headers = new Headers({'Authorization': 'bearer ' + currentUser.data.token});
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({headers: headers});
    }
  }

  // private jwtDoc() {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   if (currentUser && currentUser.token.accessToken) {
  //     let headers = new Headers({'Authorization': 'bearer ' + currentUser.token.accessToken});
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({headers: headers});
  //   }
  // }

  private header() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
    if (currentUser && currentUser.data.token) {
      var headers = new Headers({'Authorization': 'bearer ' + currentUser.data.token});
      headers.append('Content-Type', 'application/json');
      return headers;
    }
  }


}
