import { CustomerHistory, customer, contract, complain, WorkOrderDetails } from '../../models/customer-model';
import { Observable } from 'rxjs';
import { newCallDetails } from '../../models/newCustomer';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as myGlobals from '../globalPath';

@Injectable()
export class userService {

  constructor(private http: Http) { }

  checkUserNameExist(name: any): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.checkUserNameExist+ '?userName=' + `${name}`, this.jwt()).map((response: Response) => response.json());
  }

  checkUserPhoneExist(phone: any): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.checkUserPhoneExist+ '?phone=' + `${phone}`, this.jwt()).map((response: Response) => response.json());
  }

  
  checkUserEmailExist(email: any): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.checkUserEmailExist+ '?email=' + `${email}`, this.jwt()).map((response: Response) => response.json());
  }

  // private helper methods

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
    //console.log(currentUser);
    if (currentUser && currentUser.data.token) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data.token });
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({ headers: headers });
    }
  }
}
