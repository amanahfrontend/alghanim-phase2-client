import { TestBed, inject } from '@angular/core/testing';

import { CustomerCrudService } from './customer-crud.service';

describe('CustomerCrudService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerCrudService]
    });
  });

  it('should be created', inject([CustomerCrudService], (service: CustomerCrudService) => {
    expect(service).toBeTruthy();
  }));
});
