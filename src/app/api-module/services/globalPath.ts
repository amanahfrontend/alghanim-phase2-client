'use strict';

export const MainPath = 'http://sprintto-001-site1.itempurl.com';

export const API_Key = 'AIzaSyDJiuQ2hD0H4plorriRdU_VDaenjNsl_7E';
// export const BaseUrl = 'assets/jsonMocks/';
// export const BaseUrl = 'http://192.168.2.102/';
// export const BaseUrl = 'http://159.65.196.91:9000/';
// export const BaseUrl = 'http://192.168.1.18:9000/';
export const BaseUrl= 'http://157.230.52.100:9000/';

export const BaseUrlCall = BaseUrl + 'EnmaaCall2/api/';
export const BaseUrlCustomer = BaseUrl + 'EnmaaCustomer/api/';
export const BaseUrlContract = BaseUrl + 'EnmaaContract2/api/';
export const BaseUrlOrder = BaseUrl + 'EnmaaOrder2/api/';
export const BaseUrlEstimationQuotation = BaseUrl + 'EnmaaEstimationQuotation/api/';
export const BaseUrlSalary = BaseUrl + 'EnmaaEmployeeSalary2/api/';
export const BaseUrlInventory = BaseUrl + 'EnmaaInventory2/api/';
export const BaseUrlUserManagement = BaseUrl + 'EnmaaUserManagment/api/';
export const BaseUrlVehicle = BaseUrl + 'EnmaaVehicle/api/';
// export const BaseUrlInventory = BaseUrl + 'EnmaaVehicle/api/';

export const x = '';

export function testFunc(test){
  this.x = test;
}

/***************************************/
// SignalR function
/***************************************/

export function vehicleHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaVehicle/dispatchingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token
}

export function quotationHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaEstimationQuotation/quotationHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function callHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaCall/callingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function orderHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaOrder/orderingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

// http://localhost/EnmaaEstimationQuotation/quotationHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token

/***************************************/

/*---JSON mocked - Mohamed ragab---*/
export const SearchCustomer = BaseUrlCall + 'Call/Search/';
export const callPriorities = BaseUrlCall + 'CallPriority'; // Json mocking
export const callsTypes = BaseUrlCall + 'CallType'; // Json mocking
export const customerTypes = BaseUrlCustomer + 'CustomerType/GetAll'; // Json mocking
export const phoneTypes = BaseUrlCustomer + 'PhoneType/GetAll'; // Json mocking
export const locationByCustomer = BaseUrlCustomer + 'location/GetLocationByCustomerId?customerId='; // Json mocking
export const callsHistory = BaseUrlCall + 'Call/GetAll'; // Json mocking
export const callsLogs = BaseUrlCall + 'CallLog/GetByCall/'; // Json mocking
export const status = BaseUrlCall + 'CallStatus'; // Json mocking
export const item = BaseUrlEstimationQuotation + 'EstimationItem/SearchItems/'; // Json mocking
export const itemSearch = BaseUrlInventory + 'item/GetByCode/'; // Json mocking
export const estimations = BaseUrlEstimationQuotation + 'Estimation'; // Json mocking
export const searchByPhoneOrName = BaseUrlEstimationQuotation + 'Estimation/Search/'; // Json mocking
export const estimationDetails = BaseUrlEstimationQuotation + 'Estimation/Get/'; // Json mocking
export const quotations = BaseUrlEstimationQuotation + 'Quotation/GetAll'; // Json mocking
export const searchQuotation = BaseUrlEstimationQuotation + 'Quotation/Search/'; // Json mocking
export const quotationbyId = BaseUrlEstimationQuotation + 'Quotation/Get/'; // Json mocking
export const quotationbyRefNo = BaseUrlEstimationQuotation + 'Quotation/GetByRef/'; // Json mocking
export const contractTypes = BaseUrlContract + 'ContractType/GetAll'; // Json mocking
export const orderTypes = BaseUrlOrder + 'OrderType/GetAll'; // Json mocking
export const orderPriorities = BaseUrlOrder + 'OrderPriority/GetAll'; // Json mocking
export const searchContract = BaseUrlContract + 'Contract/search/'; // Json mocking
export const allContracts = BaseUrlContract + 'Contract/GetAll'; // Json mocking
export const contractById = BaseUrlContract + 'Contract/Get/'; // Json mocking
export const roles = BaseUrlUserManagement + 'Role/getall'; // Json mocking
export const users = BaseUrlUserManagement + 'User/getall'; // Json mocking
export const allOrders = BaseUrlOrder + 'Order/GetAll'; // Json mocking
export const searchOrders = BaseUrlOrder + 'Order/Search/'; // Json mocking
export const orderStatus = BaseUrlOrder + 'OrderStatus/GetAll'; // Json mocking

export const allPreventive = BaseUrlContract + 'Contract/GetAllPreventiveMaintainence';

export const GetCallDetails = BaseUrlCall + 'Call/Get/';

export const GetCustomerDetailsByID = BaseUrlCall + 'Call/Get/';

export const contractByCustomer = BaseUrlContract + 'Contract/GetByCustomer/';

export const orderByCustomer = BaseUrlOrder + 'Order/SearchByCustomerId/';

export const callrByCustomer = BaseUrlCall + 'Call/Search/';

export const allLocation = BaseUrlCustomer + 'location/getall';

export const allGovernorates = BaseUrlCustomer + 'location/GetAllGovernorates';

export const allAreas = BaseUrlCustomer + 'location/GetAreas';

export const allBlocks = BaseUrlCustomer + 'location/GetBlocks';

export const allStreets = BaseUrlCustomer + 'location/GetStreets';

export const allGetLocationByPaci = BaseUrlCustomer + 'location/GetLocationByPaci?paciNumber=';

export const allDispatchers = BaseUrlUserManagement + 'User/GetDispatchers';

export const unAssignedTecs = BaseUrlOrder +
  'AssignedTechnicans/GetUnAssignedTechnicans';

export const assignedTecs = BaseUrlOrder + 'AssignedTechnicans/GetTechnicansByDispatcherId/';

export const ordersByDis = BaseUrlOrder + 'AssignedTechnicans/GetDispatcher/';

export const assignedItems = BaseUrlInventory + 'TechnicanAssignedItems/GetAssignedItemsByTechnican/';

export const usedItems = BaseUrlInventory + 'TechnicanUsedItems/GetUsedItemsByTechnican/';

export const allTecs = BaseUrlUserManagement + 'User/GetTechnicans';

export const allCustomers = BaseUrlCustomer + 'Customer/GetAll';

/*----- Started 12/12 services mocks -----*/

export const orderProgressUrl = BaseUrlOrder + 'OrderProgress/GetByOrderId/';

export const progressStatuses = BaseUrlOrder + 'ProgressStatus/GetAll';

export const getDiByProb = BaseUrlOrder + 'OrderDistributionCriteria/GetAllDistribution';

export const allProbs = BaseUrlOrder + 'OrderProblem/GetAll';

export const removeDisWithProb = BaseUrlOrder + '/OrderDistributionCriteria/Delete/';

export const getDisWithAllProbs = BaseUrlOrder + 'OrderDistributionCriteria/GetByDispatcherId/';

export const getAllVehicles = BaseUrlVehicle + 'Vehicle/GetVehicles';

export const getAllItems = BaseUrlInventory + 'Item';

export const allOrderProgressUrl = BaseUrlOrder + 'ProgressStatus/GetAll';

export const allOrderProblemsUrl = BaseUrlOrder + 'OrderProblem/GetAll';

export const getCustomerDetails = BaseUrlCustomer + 'Customer/Get/';

/*-------------------post--------------------*/

export const logPostUrl = BaseUrlCall + 'CallLog/Add'; // Json mocking
export const estimationPostUrl = BaseUrlEstimationQuotation + 'Estimation/Add'; // Json mocking
export const callPostUrl = BaseUrlCall + 'Call/Add'; // Json mocking
export const generateQuotationUrl = BaseUrlEstimationQuotation + 'Quotation/Add'; // Json mocking
export const generateContractUrl = BaseUrlContract + 'Contract/Add'; // Json mocking
export const newUserUrl = BaseUrlUserManagement + 'User/Add'; // Json mocking
export const itemsExcelSheetUrl = BaseUrlInventory + 'ProcessItem/UpdateSalaries'; // Json mocking
export const salaryExcelSheetUrl = BaseUrlSalary + 'ProcessEmployeeSalary/UpdateSalaries'; // Json mocking
export const postNewOrderUrl = BaseUrlOrder + 'Order/Add'; // Json mocking
export const postCustomerTypesUrl = BaseUrlCustomer + 'CustomerType/Add'; // Json mocking
export const postCallsTypesUrl = BaseUrlCall + 'CallType/Add'; // Json mocking
export const postCallPrioritiesUrl = BaseUrlCall + 'CallPriority/Add'; // Json mocking
export const postPhoneTypesUrl = BaseUrlCustomer + 'PhoneType/Add'; // Json mocking
export const postActionStateUrl = BaseUrlCall + 'CallStatus/Add'; // Json mocking
export const updateStatusUrl = BaseUrlCall + 'CallStatus/Update';

export const postContractTypeUrl = BaseUrlContract + 'ContractType/Add'; // Json mocking
export const postRoleUrl = BaseUrlUserManagement + 'Role/Add'; // Json mocking
export const postOrderTypeUrl = BaseUrlOrder + 'OrderType/Add'; // Json mocking
export const postOrderPriorityUrl = BaseUrlOrder + 'OrderPriority/Add'; // Json mocking
export const postOrderStateUrl = BaseUrlOrder + 'OrderStatus/Add'; // Json mocking
export const postTecForDist = BaseUrlOrder + 'AssignedTechnicans/Add'; // Json mocking
export const orderByTec = BaseUrlOrder + 'Order/AssignOrderToTechnican';
export const unAssignedOrder = BaseUrlOrder + 'Order/UnAssignOrderToTechnican/';
export const tecAvailable = BaseUrlUserManagement + 'User/ChangeUserAvailability';

// new urls 12/12/2017
export const postPreferedVisitDate = BaseUrlOrder + 'Order/SetPreferedVisitTime';
export const transferToDispatcherUrl = BaseUrlOrder + 'Order/TransferOrderToDispatcher';
export const postProgressUrl = BaseUrlOrder + 'Order/AddOrderProgress';
export const postDispatcherWithProblemsUrl = BaseUrlOrder + 'OrderDistributionCriteria/AssignCriteriaToDispatchers';
export const filterOrdersUrl = BaseUrlOrder + 'Order/GetFilteredOrderByDispatcherId';
export const filterOrdersMapUrl = BaseUrlOrder + 'Order/GetMapFilteredOrderByDispatcherId';
export const getOrdersByDis = BaseUrlOrder + 'AssignedTechnicans/GetOrderByDispatcher/';

export const assignItemUrl = BaseUrlInventory + 'TechnicanAssignedItems/AssignItem';

export const quotationWordUrl = BaseUrlEstimationQuotation + 'Quotation/GenerateWord/';

export const filterUsedItems = BaseUrlInventory + 'TechnicanUsedItems/Filter';

// ***********************filter preventive***************

export const filterPreventiveUrl = BaseUrlContract + 'Contract/FilterPreventiveMaintainence';

export const filterEstimationsUrl = BaseUrlEstimationQuotation + 'Estimation/Filter';

export const filterQuotationsUrl = BaseUrlEstimationQuotation + 'Quotation/Filter';

export const postCustomerUrl = BaseUrlCustomer + 'Customer/Add';

export const assignOrdersToDispatcherUrl = BaseUrlOrder + 'Order/AssignOrdersToDispatcher';

export const assignOrdersToTecUrl = BaseUrlOrder + 'Order/AssignOrdersToTechnican';

export const assignedItemSearch = BaseUrlInventory + 'TechnicanAssignedItems/ChekItemExistForTechnican';

export const postOrderProgress = BaseUrlOrder + 'ProgressStatus/Add';

export const postOrderProblems = BaseUrlOrder + 'OrderProblem/Add';

export const postNewCustomerLocationUrl = BaseUrlCustomer + 'location/add';

export const removeProgressStatusUrl = BaseUrlOrder + 'ProgressStatus/Delete/';

export const removeProblemUrl = BaseUrlOrder + 'OrderProblem/Delete/';


/*-------------- Delete -------------------*/

export const deletEstimationUrl = BaseUrlEstimationQuotation + 'Estimation/Delete/'; // Json mocking
export const deleteQuotationUrl = BaseUrlEstimationQuotation + 'Quotation/Delete/'; // Json mocking
export const deleteContractUrl = BaseUrlContract + 'Contract/Delete/'; // Json mocking
export const deleteCustomertypeUrl = BaseUrlCustomer + 'CustomerType/Delete/'; // Json mocking
export const deleteCallTypeUrl = BaseUrlCall + 'CallType/Delete/'; // Json mocking
export const deletePrioritiesUrl = BaseUrlCall + 'CallPriority/Delete/'; // Json mocking
export const deletePhoneTypeUrl = BaseUrlCustomer + 'PhoneType/Delete/'; // Json mocking
export const deleteStatusUrl = BaseUrlCall + 'CallStatus/Delete/'; // Json mocking
export const deleteContractTypeUrl = BaseUrlContract + 'ContractType/Delete/'; // Json mocking
export const deleteRoleUrl = BaseUrlUserManagement + 'Role/Delete/'; // Json mocking
export const deleteUserUrl = BaseUrlUserManagement + 'User/Delete/'; // Json mocking
export const deleteOrderUrl = BaseUrlOrder + 'Order/Delete/'; // Json mocking
export const deleteOrderStatusUrl = BaseUrlOrder + 'OrderStatus/Delete/'; // Json mocking
export const deleteOrderTypeUrl = BaseUrlOrder + 'OrderType/Delete/'; // Json mocking
export const deleteOrderPriorityUrl = BaseUrlOrder + 'OrderPriority/Delete/'; // Json mocking
export const deleteTecFromDis = BaseUrlOrder + 'AssignedTechnicans/DeleteAssignedTechnican/'; // Json mocking

/*----------------- PUT -------------------*/

export const updateEstimationUrl = BaseUrlEstimationQuotation + 'Estimation/Update';
export const updateCustomerTypeUrl = BaseUrlCustomer + 'CustomerType/Update';
export const updateCallTypeUrl = BaseUrlCall + 'CallType/Update';
export const updatePriorityUrl = BaseUrlCall + 'CallPriority/Update';
export const updatePhoneTypeUrl = BaseUrlCustomer + 'PhoneType/Update';
export const updateContractTypeUrl = BaseUrlContract + 'ContractType/Update';
export const updateRoleUrl = BaseUrlUserManagement + 'Role/Update';
export const updateUserUrl = BaseUrlUserManagement + 'User/Update';
export const updateOrderUrl = BaseUrlOrder + 'Order/Update';
export const updateOrderStatusUrl = BaseUrlOrder + 'OrderStatus/Update';
export const updateOrderPriorityUrl = BaseUrlOrder + 'OrderPriority/Update';
export const updateOrderTypeUrl = BaseUrlOrder + 'OrderType/Update';
export const updateOrderProgressUrl = BaseUrlOrder + 'ProgressStatus/Update';
export const updateOrderProblemsUrl = BaseUrlOrder + 'OrderProblem/Update';

export const updateCustomerUrl = BaseUrlCustomer + 'Customer/Update';

export const updateLocationUrl = BaseUrlCustomer + 'location/update';


//*************************************************************
//*************************************************************
//*************************************************************
// -------------------- Customers URLs ------------------------

// export const SearchCustomer = MainPath + '/api/customer/SearchCustomer?searchToken=';
export const CreatenewCustomer = MainPath + '/api/customer/CreateDetailedCustomer';
// export const GetCustomerDetailsByID = MainPath + '/api/customer/CustomerHistory/';
export const EditCustomerInfo = MainPath + '/api/customer/Update';
export const EditContract = MainPath + '/api/contract/Update';
export const AddContract = MainPath + '/api/contract/add';
export const AddNewWorkOrder = MainPath + '/api/WorkOrder/AddWorkOrder';
export const AddLocation = MainPath + '/api/location/add';

export const EditComplain = MainPath + '/api/complain/Update';
export const AddComplain = MainPath + '/api/complain/add';
export const EditWorkOrder = MainPath + '/api/WorkOrder/EditWorkOrder';

//-------------------- Lookups URls -----------------------------
export const allCustomerTypes = MainPath + '/api/CustomerType/getall';
export const allContractType = MainPath + '/api/ContractType/getall';
export const allWorkItems = MainPath + '/api/Item/GetAll';
// export const allLocation = MainPath + '/api/location/getall';
export const allFactories = MainPath + '/api/Factory/GetAll';
// export const allGovernorates = MainPath + '/api/location/GetAllGovernorates';

// export const allAreas = MainPath + '/api/location/GetAreas';
// export const allBlocks = MainPath + '/api/location/GetBlocks';
// export const allStreets = MainPath + '/api/location/GetStreets';
// export const allGetLocationByPaci = MainPath + '/api/location/GetLocationByPaci?paciNumber=';
export const allStatus = MainPath + '/api/Status/getall';


export const editStatus = MainPath + '/api/Status/Update';
export const AddStatus = MainPath + '/api/Status/Add';
export const DeleteStatus = MainPath + '/api/Status/Delete';

export const allRole = MainPath + '/api/Role/getall';
export const editRole = MainPath + '/api/Role/Update';
export const AddRole = MainPath + '/api/Role/Add';
export const DeleteRole = MainPath + '/api/Role/Delete';

export const allPeriorities = MainPath + '/api/Priority/getall';
export const editPeriorities = MainPath + '/api/Priority/Update';
export const AddPeriorities = MainPath + '/api/Priority/Add';
export const DeletePeriorities = MainPath + '/api/Priority/Delete';

export const editCustomerType = MainPath + '/api/CustomerType/Update';
export const AddCustomerType = MainPath + '/api/CustomerType/Add';
export const DeleteCustomerType = MainPath + '/api/CustomerType/Delete';

export const editContractType = MainPath + '/api/ContractType/Update';
export const AddContractType = MainPath + '/api/ContractType/Add';
export const DeleteContractType = MainPath + '/api/ContractType/Delete';

export const allEquipmentType = MainPath + '/api/EquipmentType/getall';
export const editEquipmentType = MainPath + '/api/EquipmentType/Update';
export const AddEquipmentType = MainPath + '/api/EquipmentType/Add';
export const DeleteEquipmentType = MainPath + '/api/EquipmentType/Delete';

export const allEquipment = MainPath + '/api/Equipment/getall';
export const editEquipment = MainPath + '/api/Equipment/Update';
export const AddEquipment = MainPath + '/api/Equipment/Add';
export const DeleteEquipment = MainPath + '/api/Equipment/Delete';

export const editItem = MainPath + '/api/Item/Update';
export const AddItem = MainPath + '/api/Item/Add';
export const DeleteItem = MainPath + '/api/Item/Delete';

export const editFactory = MainPath + '/api/Factory/Update';
export const AddFactory = MainPath + '/api/Factory/Add';
export const DeleteFactory = MainPath + '/api/Factory/Delete';

export const alluser = MainPath + '/api/User/getall';
export const Adduser = MainPath + '/api/User/Add';
export const Deleteuser = MainPath + '/api/User/Delete';

export const DeleteComplain = MainPath + '/api/complain/delete?id=';
export const DeleteContract = MainPath + '/api/contract/delete?id=';
export const DeleteWorkOrder = MainPath + '/api/workorder/delete?id=';

export const UsernameExists = MainPath + '/api/User/UsernameExists?username=';
export const EmailExists = MainPath + '/api/User/EmailExists?email=';
// ---------------------------Work Order Management page -------------------------

export const AllWorkOrdersDispature = MainPath + '/api/WorkOrder/GetAllSuborders';
export const AllWorkOrdersMovementController = MainPath + '/api/WorkOrder/GetByMovementController';
export const AvailableEquipmentInFactory = MainPath + '/api/Equipment/GetAvailableEquipmentInFactory?factoryId=';
export const UpdateAssignmentToWorkOrder = MainPath + '/api/Equipment/UpdateAssignmentToWorkOrder';
export const UpdateSubOrder = MainPath + '/api/WorkOrder/UpdateSubOrder';
export const reassinFactory = MainPath + '/api/WorkOrder/CanAssignToFactory?workorderId=';
export const AssignmentToFactory = MainPath + '/api/WorkOrder/UpdateOrderAssignmentToFactory?orderId=';
export const JobDetails = MainPath + '/api/job/GetByWorkOrder?workorderId=';
export const AvailableVehicleInFactory = MainPath + '/api/vehicle/GetAvailableVehicleInFactory?factoryId=';
export const AddJop = MainPath + '/api/job/add';
export const CancelJop = MainPath + '/api/job/Cancel?jobId=';
export const ReassignJop = MainPath + '/api/job/Reassign?jobId=';
export const AllWorkOrder = MainPath + '/api/WorkOrder/getall';
export const checkCustomerNameExist = MainPath + '/api/customer/IsCustomerNameExist?name=';
export const checkCustomerphoneExist = MainPath + '/api/customer/IsPhoneExist?phone=';
export const AllVehicles = MainPath + '/api/vehicle/getall';


export const NotificationOrders = MainPath + '/api/WorkOrder/getall';
export const RemindOrders = MainPath + '/api/WorkOrder/getall';



/////////////////////Aml Api//////////////////////////////
export const BaseUrlIdentity = BaseUrl + 'identityapi/api/';
export const BaseUrlDataManagement = BaseUrl + 'dataManagementaPI/api/';
export const BaseUrlUserManagementNew = BaseUrl + 'userManagementaPI/api/';
export const BaseUrlOrderManagementNew = BaseUrl + 'Orderapi/api/';

export const loginUrl = 'User/Login';
//////////////////////DataManagement/////////////////////////////////
//////////////Get/////////////
export const areasUrl = 'Areas/GetAll';
export const areasPaginatedUrl = 'Areas/GetAllPaginated';
export const attendanceStatusUrl = 'AttendanceStates/GetAll';
export const availabilitysUrl = 'Availability/GetAll';
export const buildingTypesUrl= 'BuildingTypes/GetAll';
export const companyCodeUrl= 'CompanyCode/GetAll';
export const contractTypesUrl= 'ContractTypes/GetAll';
export const costCenterUrl = 'CostCenter/GetAll';
export const divisionUrl = 'Division/GetAll';
export const governoratesUrl= 'Governorates/GetAll';
export const shiftUrl= 'Shift/GetAll';
export const languagesUrl = 'SupportedLanguages/GetAll';
export const rolesUrl = 'Role/GetAll'
export const adminUserUrl = 'User/GetAdminUsers'

/////////GetById///////////////
export const areaLanguageByIdUrl = 'Lang_Areas/GetAllLanguagesByAreaId/'
export const attendanceStatesByIdUrl = 'Lang_AttendanceStates/GetAllLanguagesByAttendanceStatesId/'
export const availabilityByIdUrl = 'Lang_Availability/GetAllLanguagesByAvailabilityId/'
export const buildingTypesByIdUrl = 'Lang_BuildingTypes/GetAllLanguagesByBuildingTypesId/'
export const companyCodeByIdUrl = 'Lang_CompanyCode/GetAllLanguagesByCompanyCodeId/'
export const contractTypesByIdUrl = 'Lang_ContractTypes/GetAllLanguagesByContractTypesId/'
export const CostCentersByIdUrl = 'Lang_CostCenter/GetAllLanguagesByCostCenterId/'
export const divisionByIdUrl = 'Lang_Division/GetAllLanguagesByDivisionId/'
export const governoratesByIdUrl = 'Lang_Governorates/GetAllLanguagesByGovernorateId/'
export const ShiftByIdUrl ='Lang_Shift/GetAllLanguagesByShiftId/'

//////////////Post/////////////
export const postAreasUrl = 'Areas/Add'
export const postAttendanceStates = 'AttendanceStates/Add'
export const postAvailability = 'Availability/Add'
export const postBuildingTypes = 'BuildingTypes/Add'
export const postCompanyCode = 'CompanyCode/Add'
export const postContractTypes = 'ContractTypes/Add'
export const postCostCenter = 'CostCenter/Add'
export const postDivision = 'Division/Add'
export const postGovernorates = 'Governorates/Add'
export const postShift = 'Shift/Add'
export const postLanguages = 'SupportedLanguages/Add'

export const postAreaLanguage = 'Lang_Areas/AddMulti'
export const postAttendanceStatesLanguage = 'Lang_AttendanceStates/AddMulti'
export const postAvailabilityLanguage = 'Lang_Availability/AddMulti'
export const postBuildingTypesLanguage = 'Lang_BuildingTypes/AddMulti'
export const postCompanyCodeLanguage = 'Lang_CompanyCode/AddMulti'
export const postContractTypesLanguage = 'Lang_ContractTypes/AddMulti'
export const postCostCenterLanguage = 'Lang_CostCenter/AddMulti'
export const postDivisionLanguage = 'Lang_Division/AddMulti'
export const postGovernoratesLanguage = 'Lang_Governorates/AddMulti'
export const postShiftLanguage = 'Lang_Shift/AddMulti'


//////////////put///////////
export const putAreasUrl = 'Areas/Update'
export const putAttendanceStatesUrl = 'AttendanceStates/Update'
export const putAvailabilityUrl = 'Availability/Update'
export const putBuildingTypesUrl = 'BuildingTypes/Update'
export const putCompanyCodeUrl = 'CompanyCode/Update'
export const putContractTypesUrl = 'ContractTypes/Update'
export const putCostCenterUrl = 'CostCenter/Update'
export const putDivisionUrl = 'Division/Update'
export const putGovernoratesUrl = 'Governorates/Update'
export const putShiftUrl = 'Shift/Update'
export const putLanguagesUrl = 'SupportedLanguages/Update'

export const putAreaLanguageUrl = 'Lang_Areas/UpdateLanguages'
export const putAttendanceStatesLanguageUrl = 'Lang_AttendanceStates/UpdateLanguages'
export const putAvailabilityLanguageUrl = 'Lang_Availability/UpdateLanguages'
export const putBuildingTypesLanguageUrl = 'Lang_BuildingTypes/UpdateLanguages'
export const putCompanyCodeLanguageUrl = 'Lang_CompanyCode/UpdateLanguages'
export const putContractTypesLanguageUrl = 'Lang_ContractTypes/UpdateLanguages'
export const putLang_CostCenterLanguageUrl = 'Lang_CostCenter/UpdateLanguages'
export const putDivisionLanguageUrl = 'Lang_Division/UpdateLanguages'
export const putGovernoratesLanguageUrl = 'Lang_Governorates/UpdateLanguages'
export const putShiftLanguageUrl = 'Lang_Shift/UpdateLanguages'


/////////////delete////////////////
export const deleteAreaUrl = 'Areas/Delete/'
export const deleteAttendanceStatesUrl = 'AttendanceStates/Delete/'
export const deleteAvailabilityUrl = 'Availability/Delete/'
export const deleteBuildingTypesUrl = 'BuildingTypes/Delete/'
export const deleteCompanyCodeUrl = 'CompanyCode/Delete/'
export const deleteContractTypesUrl = 'ContractTypes/Delete/'
export const deleteCostCenterUrl = 'CostCenter/Delete/'
export const deleteDivisionUrl = 'Division/Delete/'
export const deleteGovernoratesUrl = 'Governorates/Delete/'
export const deleteShiftUrl = 'Shift/Delete/'
export const deleteLanguagesUrl = 'SupportedLanguages/Delete/'

export const deleteAreaLanguagesUrl = 'Lang_Areas/Delete/'
export const deleteAttendanceStatesLanguagesUrl = 'Lang_AttendanceStates/Delete/'


///////////////////User Managent////////////////////
////////Get//////////
export const getDispatcherUrl = 'Dispatcher/GetAll'
export const getDriverUrl = 'Driver/GetAll'
export const getSuperVisorUrl = 'Supervisor/GetAll'
export const getEngineerUrl = 'Engineer/GetAll'
export const getTechnicianUrl = 'Technician/GetAll'
export const getForemanUrl = 'Foreman/GetAll'
export const getManagerUrl ='Manager/GetAll'
export const getMaterialUrl = 'Material/GetAll'

/////////Post/////////////
export const postUserUrl = 'User/Add'
export const postUserDispatcherUrl = 'Dispatcher/Add'
export const postSupervisorUrl= 'Supervisor/Add'
export const postDriverUrl= 'Driver/Add'
export const postEngineerUrl= 'Engineer/Add'
export const postTechnicianUrl= 'Technician/Add'
export const postForemanUrl= 'Foreman/Add'
export const postManagerUrl ='Manager/Add'
export const postMatrialController ='Material/Add'


//////////Update//////////////
export const putUserUrl = 'User/Update'
export const putUserDispatcherUrl = 'Dispatcher/Update'
export const putUserSupervisorUrl = 'Supervisor/Update'
export const putUserDriverUrl = 'Driver/Update'
export const putUserEngineerUrl = 'Engineer/Update'
export const putUserTechnicianUrl = 'Technician/Update'
export const putUserForemanUrl = 'Foreman/Update'
export const PutUserManagerUrl ='Manager/Update'
export const PutUserMatrialUrl ='Material/Update'


//////////////GetById///////////////
export const getDispatcherUserById= 'Dispatcher/GetDispatcherUser/'
export const getSupervisorUserById='Supervisor/GetSupervisorUser/'
export const getDriverUserById= 'Driver/GetDriverUser/'
export const getEngineerUserById= 'Engineer/GetEngineerUser/'
export const getTechnicianUserById= 'Technician/GetTechnicianUser/'
export const getForemanUserById= 'Foreman/GetForemanUser/'
export const getManagerById = 'Manager/GetByIds'
export const getControllerById ='Material/GetByIds'


///////////GetByAnotherRole
export const getSupervisorUserBydivison='Supervisor/GetSupervisorsByDivisionId/';
export const getEngineerUserBydivison='Engineer/GetEngineerUsersByDivisionId/';
export const getDriverUserBydivison='Driver/GetDriverUsersByDivisionId/';
export const getDispatchersUserBySuper='Dispatcher/GetDispatcherUsersBySupervisorId/';
export const getFormanUserByDispatcher='Foreman/GetForemansByDispatcherId/';


////////////////Identity/////////////////
export const getrUserById= 'User/Get?id=';
export const checkUserNameExist = 'User/IsUsernameExist';
export const checkUserPhoneExist = 'User/IsPhoneExist';
export const checkUserEmailExist = 'User/IsEmailExist';

///////////////////Team Management////////////////////////
/////////Get/////////
export const getUnassignedMempersUrl = 'TeamMember/GetUnassignedMember';
export const getByDispatcherByIdUrl = 'Team/GetByDispatcherId/';
export const getTeams ='Team/GetAllWithDispatcher'


////////////Post///////////////
export const postTeam = 'Team/Add'
export const assignTeamMember = 'TeamMember/assignedMemberToTeam'
export const reassignTeamToDispatcher = 'Team/ReassignTeamToDispatcher'
export const unassignedMember = 'TeamMember/UnassignedMember'
export const postProblemArea = 'DispatcherSettings/Add'


////////////////////////Dispatcher Setting////////////
export const OrderProblem ='OrderProblem/GetAll'
export const disSettingUrl ='DispatcherSettings/GetAll'
export const getDispatcherSettingById = 'DispatcherSettings/GetbyDispatcherId/{0}'
export const deleteDispatcherSetting = 'DispatcherSettings/Delete/{0}'
export const postDispatcherAllProblemsandArea = 'DispatcherSettings/AddWithAreasAndOrderProblems'


//////////////////////OrderManagement/////////////////////////////////
//////////////Get/////////////
export const unAssignedOrdersForDispatcherUrl = 'Order/GetUnAssignedOrdersForDispatcher?dispatcherId=';
export const assignedOrdersForDispatcherUrl =  'Order/GetAssignedOrdersForDispatcher?dispatcherId='
export const assigndOrdersUrl = 'Order/Assign';
export const unAssigndOrdersUrl = 'Order/UnAssign'
 


