import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {NavigationEnd, Router} from "@angular/router";

// import {Subject} from "rxjs/Subject";

@Injectable()
export class UtilitiesService {
  existedCustomer = new BehaviorSubject(<any>{});
  routingData = new BehaviorSubject(<any>{});
  toggleFullWidth: boolean;
  currentSearch: any;
  // currentSearchQuotation: any;
  // currentSearchContract: any;
  // currentNotificationOrder: any;
  routingFromAndHaveSearch: boolean;
  savedNotificationText = new BehaviorSubject(<any>'');
  previousUrl: any;
  statusColors: any =
    [
      {
        name: 'open',
        color: '#2400ff'
      },
      {
        name: 'dispatched',
        color: '#ff0000'
      },
      {
        name: 'in progress',
        color: '#ffea00'
      },
      {
        name: 'completed',
        color: '#0c1676'
      },
      {
        name: 'cancelled',
        color: '#ff1700'
      }
    ];

  constructor(private router: Router) {
    // this.router.events
    //   .filter(event => event instanceof NavigationEnd)
    //   .subscribe(e => {
    //     console.log('prev:', this.previousUrl);
    //     this.previousUrl = e['url'];
    //   });
  }

  setSavedNotificationText(value) {
    this.savedNotificationText.next(value);
  }

  updateCurrentExistedCustomer(newExistedCustomer) {
    this.existedCustomer.next(newExistedCustomer);
  }

  unauthrizedAction() {
    this.router.navigate(['/login']);
  }

  setRoutingDataPassed(data) {
    this.routingData.next(data);
  }

  hideGrowl(time) {
    return setTimeout(() => {
      return [];
    }, time)
  }

  convertDatetoNormal(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${day}/${month}/${year}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  convertDateForSearchBinding(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${year}-${month}-${day}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  printComponent(elementId) {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=1000px,width=1000px');

    var divToPrint = document.getElementById(elementId);
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  setOrdersColorByState(orders) {
    return orders.map((order) => {
      let orderStatus = (order.status && order.status.name.toLowerCase()) || (order.orderStatus && order.orderStatus.name.toLowerCase());
      return this.statusColors.map((color) => {
        if (color.name == orderStatus) {
          return order.color = color.color;
        }
      });
    });
  }
}
