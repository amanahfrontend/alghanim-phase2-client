import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-edit-dis-and-prob-modal',
  templateUrl: './edit-dis-and-prob-modal.component.html',
  styleUrls: ['./edit-dis-and-prob-modal.component.css']
})
export class EditDisAndProbModalComponent implements OnInit {
  // area: any[];
  @Input() data;
  problems: any[];
  dispatcherDetails: any;
  dispatcherDetailsFormated: any[];
  areas: any[];
  governorates: any[];
  toggleLoading: boolean;
  newProblems: any[];
  cornerMessage: any[];
  objToPost: any = {};


  constructor(private activeModal: NgbActiveModal, private _lookup: LookupService) {
  }

  ngOnInit() {
    this.newProblems = [];
    this.dispatcherDetails = {};
    this.dispatcherDetailsFormated = [];
    console.log(this.data);
    this.getAllProblems();
    if (this.data.area != '-') {
      this.getDispatcherAllProblems(this.data.dispatcherId);
      this.toggleLoading = true;
    }
    // this.getGovernorate();
    console.log(this.data);
    this.getAllAreas();
  }

  addNewProblem() {
    console.log('NEW PROBLEM');
    console.log(this.newProblems);
    this.newProblems.push({
      area: '',
      problems: []
    });
    console.log(this.newProblems);
  }

  removeNewProblem(item) {
    let removedFlag: boolean;
    this.newProblems.map((problem) => {
      if (JSON.stringify(item) == JSON.stringify(problem) && !removedFlag) {
        this.newProblems.splice(this.newProblems.indexOf(item), 1)
      }
    })
  }

  getGovernorate() {
    this._lookup.getGovernorates().subscribe((governorates) => {
      console.log(governorates)
      if (governorates.data[0].name == 'Not Found In Paci') {
        console.log('will retry');
        this.getGovernorate();
      } else {
        console.log('got governorates');
        this.governorates = governorates.data;
        if (this.dispatcherDetailsFormated.length) {
          this.dispatcherDetailsFormated.map((dispatcherDetail) => {
            this.getAreas(dispatcherDetail);
          })
        } else {
          this.newProblems.map((prob) => {
            this.getAreas(prob);
          })
        }
      }
    },
      err => {

      })
  }

  getAllAreas() {
    console.log("getAllAreas : ", this.areas)

    this._lookup.getAllAreasDataManagement().subscribe((data) => {
      this.toggleLoading = false;
      this.areas = data.data;
      this.areas.map((are, i) => {
        this.areas[i] = {
          label: are.name,
          value: are.name
        }
      })

    },
      err => {
      })
  }










  getAreas(item) {
    let usedItem = item;
    console.log(usedItem);
    this._lookup.getAllAreasDataManagement().subscribe((data) => {
      this.toggleLoading = false;
      this.areas = data.data;

    },
      err => {
      })

    // this.toggleLoading = true;
    // let id = this.selectId(usedItem.governorate, this.governorates);
    // console.log(id);
    // this._lookup.GetallAreas(id).subscribe((areas) => {
    //     if (areas[0].name == 'Not Found In Paci') {
    //       console.log('will retry Areas');
    //       this.getAreas(usedItem);
    //     } else {
    //       // console.log('got Areas');
    //       // console.log(areas);
    //       if (this.dispatcherDetailsFormated.length) {
    //         this.dispatcherDetailsFormated.map((dis) => {
    //           if (dis.governorate == usedItem.governorate) {
    //             dis.areas = areas;
    //             this.toggleLoading = false;
    //           }
    //         });
    //       }
    //       else {
    //         this.newProblems.map((prob) => {
    //           if (prob.governorate == usedItem.governorate) {
    //             prob.areas = areas;
    //             this.toggleLoading = false;
    //           }
    //         })
    //       }
    //       if (this.newProblems.length) {
    //         this.newProblems.map((prob) => {
    //           if (prob.governorate == usedItem.governorate) {
    //             prob.areas = areas;
    //             this.toggleLoading = false;
    //           }
    //         })
    //       }
    //       // console.log(usedItem.areas);
    //       console.log(this.dispatcherDetailsFormated);
    //     }
    //   },
    //   err => {

    //   });
  }

  selectId(name, list) {
    console.log(name);
    console.log(list);
    return list.filter((item) => {
      return item.name == name;
    })[0].id;
  }

  getAllProblems() {
    this._lookup.getAllOrderProblem().subscribe((problems) => {
      this.problems = problems.data;
      console.log(this.problems)
      this.problems.map((prob, i) => {
        this.problems[i] = {
          label: prob.name,
          value: prob.name
        }
      });
      console.log(this.problems)
    },
      err => {
      });
  }

  getDispatcherAllProblems(id) {
    this._lookup.getDispatcherSettingById(id).subscribe((disWithAllProbs) => {
      this.dispatcherDetails = disWithAllProbs;
      this.dispatcherDetails.map((detail) => {
        console.log(detail);
        let area = detail.area;
        if (this.dispatcherDetailsFormated.length) {
          let areaFoundFlag = false;
          this.dispatcherDetailsFormated.map((detailFormated, i) => {
            if (detailFormated.area == area) {
              detailFormated.problems.push({
                id: detail.orderProblems.problemId,
                name: detail.orderProblems.problemName
              });
              areaFoundFlag = true;
            }
            if (!areaFoundFlag && (i == this.dispatcherDetailsFormated.length - 1)) {
              debugger;
              let currentProblems = [];

              detail.orderProblems.map(prob => {
                currentProblems.push(prob.problemName
                )
              });

              // detail.orderProblems.map(prob =>{
              //   currentProblems.push({
              //     id: prob.problemId,
              //     name: prob.problemName
              //   })
              //   });
              this.dispatcherDetailsFormated.push({
                area: detail.areaName,
                problems: currentProblems
              })
            }
          });
        } else {
          debugger;
          let currentProblems = [];

          detail.orderProblems.map(prob => {
            currentProblems.push(prob.problemName
            )
          });
          this.dispatcherDetailsFormated.push({
            area: detail.areaName,
            problems: currentProblems
          });
        }
      });
      // this.selectedProbs = this.dispatcherDetailsFormated[0].problems;
      console.log("this.dispatcherDetailsFormated");
      console.log(this.dispatcherDetailsFormated);
    },
      err => {

      })
  }

  applyEdits() {

   


    console.log(this.dispatcherDetailsFormated);
    console.log('newProblems' + JSON.stringify(this.newProblems));
    let allViewProblemsAndAreas = this.dispatcherDetailsFormated.concat(this.newProblems);
    // console.log(allViewProblemsAndAreas);
    // let dispatcherToPost = [];
    // let areaProblems = [];
    // allViewProblemsAndAreas.map((area) => {
    //   let fK_OrderProblem_Ids = [];
    //   area.problems.map((problem, i) => {
    //     console.log(area.problems.length);
    //     console.log(i);
    //     fK_OrderProblem_Ids.push(problem.id);
    //     if (area.problems.length - 1 == i) {
    //       areaProblems.push({
    //         area: area.area,
    //         governorate: area.governorate,
    //         fK_OrderProblem_Ids: fK_OrderProblem_Ids
    //       })
    //     }
    //   });
    // });

    // console.log(this.data);

    // dispatcherToPost.push({
    //   dispatcherId: this.data.dispatcher.id,
    //   areaProblems: areaProblems
    // });
    this.objToPost.dispatcherId = this.data.dispatcherId
    this.objToPost.dispatcherName = this.data.dispatcherName
   // this.objToPost.divisionId = this.data.divisionId
    //this.objToPost.divisionName = this.data.divisionName

  
    this.objToPost.orderProblems =[];
    this.objToPost.areas =[];


    console.log(allViewProblemsAndAreas);
    console.log(this.problems)
    allViewProblemsAndAreas.map(prob => {
      prob.areas.map(probString => {
        this.areas.map(area => {
          console.log("all areas" , this.areas)
          if (area.label == probString) {
            let obj = {areaId:area.id , areaName:area.value};
            this.objToPost.areas.push(obj);

            // this.objToPost.orderProblemId = problem.id
            // this.objToPost.orderProblemName = problem.id
          }
        })
      })
      //  prob.area.map(probString => {
      //           this.areas.map(area => {
      //             if (area.label == probString) {
      //               this.objToPost.areaId = prob.area.id
      //               this.objToPost.areaName = prob.area
      //             }
      //           })
      //         });
 
      // prob.problems.map(probString => {
      //   this.problems.map(problem=> {
      //     if (problem.label == probString) {
      //       this.objToPost.disProblems.push(problem.id ,problem.value)
        
      //     }
      //   })
      // });

    //           // prob.area.map(probString => {
    //           //   this.problems.map(area => {
    //           //     if (area.label == probString) {
    //           //       this.objToPost.areaId = prob.area.id
    //           //       this.objToPost.areaName = prob.area
    //           //     }
    //           //   })
    //           // });
    // })

      // this.objToPost.areaId = prob.area.id
      // this.objToPost.areaName = prob.area
      prob.problems.map(probString => {
        this.problems.map(problem => {
          //console.log("this is all prop" ,this.problems )
          if (problem.label == probString) {
            let obj = {problemId:problem.id , problemName:problem.value};
            this.objToPost.orderProblems.push(obj);

            // this.objToPost.orderProblemId = problem.id
            // this.objToPost.orderProblemName = problem.id
          }
        })
      })

    })

    this._lookup.postDispatcherAllProblemsandArea(this.objToPost).subscribe((resposnse) => {
    
    });
    console.log(this.objToPost);

    // this._lookup.postProblemArea(this.objToPost).subscribe(() => {
    //   console.log('successful');
    //   this.activeModal.close();
    // },
    //   err => {
    //     console.log('failed')
    //   })
  }

  close() {
    this.activeModal.dismiss();
  }
}
