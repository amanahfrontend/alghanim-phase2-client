import {Component, OnInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {EditDisAndProbModalComponent} from "./edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";

@Component({
  selector: 'app-setting-dispatchers',
  templateUrl: './setting-dispatchers.component.html',
  styleUrls: ['./setting-dispatchers.component.css']
})

export class SettingDispatchersComponent implements OnInit {
  disWithProbs: any[];
  allDispatchers: any = [];
  toggleLoading: boolean;
  allAreas;
  allProblems:any = [];

  constructor(private lookup: LookupService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.getDispatchers();
    this.getAllProblems();
    this.getAreas();

  }

  setupAreasForFilter() {
    this.allAreas = [];
    this.disWithProbs.map((dis) => {
      if (!this.allAreas.includes(dis.area) && dis.area != '-') {
        this.allAreas.push(dis.area);
      }
    });
    this.allAreas.map((area, i) => {
      this.allAreas[i] = {
        label: area,
        value: area
      }
    });
    console.log(this.allAreas)
  }

  getAreas(){
    this.lookup.getAllAreasDataManagement().subscribe((data) => {
      this.toggleLoading = false;
      this.allAreas = data.data;
      console.log(this.allAreas);
      this.allAreas.map((area, i) => {
        this.allAreas[i] = {
          label: area.name,
          value: area.name
        }
      });
    },
      err => {
      })
  }

  getAllProblems() {
    this.lookup.getAllOrderProblem().subscribe((problems) => {
        this.allProblems = problems.data;
        console.log(this.allProblems)
        this.allProblems.map((prob, i) => {
          this.allProblems[i] = {
            label: prob.name,
            value: prob.name
          }
        });
        console.log(this.allProblems)
      },
      err => {
      })
  }

  getAllDispatchers() {
    let emptyList = [];
    this.lookup.getAllDispatchers().subscribe((dispatchers) => {
        console.log(dispatchers);
        console.log(this.disWithProbs);
        if (this.disWithProbs.length) {
          let result = this.disWithProbs.map(a => a.fK_Dispatcher_Id);
          var unique = result.filter((v, i, a) => a.indexOf(v) === i);
          this.allDispatchers = [];
          dispatchers.data.map((dis) => {
            if (!~unique.indexOf(dis.id)) {
              this.allDispatchers.push(dis);
            }
          });
          console.log(unique);
        } else {
          this.allDispatchers = dispatchers.data;
        }
        this.allDispatchers.map((dis, i) => {
          this.allDispatchers[i] = {
            fK_Dispatcher_Id: dis.id,
            dispatcher: {
              id: dis.id,
              userName: dis.userName
            },
            area: '-',
            orderProblem: {
              name: '-'
            }
          };
        });
        console.log(emptyList);
        console.log(this.allDispatchers);
        this.disWithProbs = this.disWithProbs.concat(this.allDispatchers);
        console.log(this.disWithProbs);
        // this.setupAreasForFilter();
        this.toggleLoading = false;
      },
      err => {

      })
  }

  openEditModal(dispatcher) {
    let modalRef = this.modalService.open(EditDisAndProbModalComponent);
    modalRef.componentInstance.data = dispatcher;
    modalRef.result
      .then(() => {
        console.log('then');
        this.getDispatchers();
      })
      .catch(() => {

      })
  }

  remove(dis) {
    console.log(dis);
    this.lookup.removeDisByProb(dis.id).subscribe(() => {
        console.log('success');
        this.disWithProbs = this.disWithProbs.filter((disWIthProb) => {
          return disWIthProb.id != dis.id;
        })
      },
      err => {
        console.log('failed');
      })
  }

  getDispatchers() {
    console.log('getting dispatcher');
    this.lookup.getAllDispatcherSettings().subscribe((diswithProbs) => {
        this.disWithProbs = diswithProbs.data;
        console.log(this.disWithProbs);
        this.getAllDispatchers()
      },
      err => {
        console.log('failed');
      })
  }
}
