import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dispatcher'
})
export class DispatcherPipe implements PipeTransform {

  transform(unassignedOrders: any, search: string): any {
    
    if(!unassignedOrders || !search)
    {
      return unassignedOrders;
    }
    return unassignedOrders.filter(unassignedOrders => unassignedOrders.customerName.indexOf(search) !== -1)
  }

}
