import {AuthGuardGuard} from './../api-module/guards/auth-guard.guard';
import {RouterModule, Routes} from '@angular/router';
import {SharedModuleModule} from '../shared-module/shared-module.module';
import { NgDragDropModule } from 'ng-drag-drop';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminMainPageComponent} from './admin-main-page/admin-main-page.component';
import {SimpleAdminContentComponent} from './simple-admin-content/simple-admin-content.component';
import {AdminUsersComponent} from './admin-users/admin-users.component';
import {AdminUploadResourcesComponent} from './admin-upload-resources/admin-upload-resources.component';
import {AssignTecComponent} from './assign-tec/assign-tec.component';
import {SettingDispatchersComponent} from './setting-dispatchers/setting-dispatchers.component';
import {EditDisAndProbModalComponent} from './setting-dispatchers/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component';
import {MultiSelectModule} from 'primeng/components/multiselect/multiselect';
import {userManagmentComponent} from './userManagment/userManagment.component';
import {teamManagmentComponent} from './teamManagement/teamManagement.component';
import {orderManagmentComponent} from './orderManagement/orderManagement.component';
import { FormsModule } from '@angular/forms';
import { DispatcherPipe } from './dispatcher.pipe';


const routes: Routes = [
  {
    path: '',
    component: AdminMainPageComponent,
    // canActivate: [AuthGuardGuard],
    data: {roles: ['Admin']}
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModuleModule,
    MultiSelectModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgDragDropModule.forRoot(),
   
  ],
  declarations: [AdminMainPageComponent,  DispatcherPipe,SimpleAdminContentComponent, AdminUsersComponent, AdminUploadResourcesComponent,orderManagmentComponent,
    AssignTecComponent, SettingDispatchersComponent, EditDisAndProbModalComponent,userManagmentComponent,teamManagmentComponent
  ],
  entryComponents: [
    EditDisAndProbModalComponent
  ]
})

export class AdminModule {
}
