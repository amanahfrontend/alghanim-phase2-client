import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { DragulaService } from 'ng2-dragula';
import { teamModelComponent } from '../../shared-module/shared/team/teamModel.component';
import { teamMemberModelComponent } from '../../shared-module/shared/teamMember/teamMember.component';
import { teamOrderModel } from '../../shared-module/shared/teamOrdersModel/teamOrderModel';
//import {DispatcherPipe} from '../dispatcher.pipe';
//import {teamOrderModel} from '../../shared-module/teamOrderModel/teamOrderModel';

@Component({
  selector: 'app-orderManagement',
  templateUrl: './orderManagement.component.html',
  styleUrls: ['./orderManagement.component.css'],

})


export class orderManagmentComponent implements OnInit {

  constructor(private lookUp: LookupService, private messageService: MessageService,
    private modalService: NgbModal, private utilities: UtilitiesService, private dragulaService: DragulaService) {

    this.dragulaService.drop.subscribe((value) => {
      console.log(value)
      // if (value[0]== 'memberBag') {
      this.assignOrder(value.slice(1));
      // }
      // if (value[0]== 'teamBag') {
      // this.assignTeamToDispacher(value.slice(1));
      // }
    });

    this.dragulaService.setOptions("memberBag", {
      direction: 'horizontal',
      moves: function (el: any, container: any, handle: any): any {
        if (el.classList.contains('disableDrag')) {
          return false;
        }
        return true;
      }
    });
  }

  
  unassignedOrders: any = [];
  modalRef: any;
  assignedOrders: Array<teamOrderModel> = [];
  memberObj: any = {};
  dispatchers: any = []
  memberToPost: any;
  memberTeamToPost: any;
  unasssignOrderToPost: any;
  dispatcherToPost: any;
  teams: any = [];
  dragEnabled: any;
  toggleLoading: boolean;
  userId = localStorage.getItem('AlghanimCurrentUserId');
  teamOrderToPost: any;
  dispatcherTeams: any = [];
  dispatcherTeamsFilter: any = [];
  search: string = '';
  staus: any = "Open";




  ngOnInit() {
    this.getUnassignedOrders();
    this.getAssignedOrders();
    // this.getTeamsByDis();
  }

  getUnassignedOrders() {
    this.lookUp.getAllUnAssignedOrdersForDispatcher(this.userId).subscribe(result => {
      this.unassignedOrders = result.data;
      console.log(this.unassignedOrders)
    }, error => {
      console.log('error')
    })
  }

  getAssignedOrders() {
    this.lookUp.getAllAssignedOrdersForDispatcher(this.userId).subscribe(result => {
      this.assignedOrders = result.data;
      this.assignedOrders.forEach(element => {
        element.orders.forEach(item =>{
          item.style =  this.chooseColor(item.typeName,item.createdDate);
        });
      });
     
    }, error => {
      console.log('error')
    })
  }


  ToggleStatus(staus: any) {
    this.staus = staus;
    console.log(this.staus);
  }

  getTeamsByDis() {
    // this.toggleLoading = true;
    this.lookUp.getTeamByDispatcherById(this.userId).subscribe(result => {
      // this.dispatcherTeams = result.data;
      this.assignedOrders.map((teamOrders) => {
        result.data.map((dispatcherTeam) => {
          dispatcherTeam.orders = [];
          if (dispatcherTeam.id === teamOrders.teamId) {
            console.log(dispatcherTeam.id)
            console.log(teamOrders.teamId)
            // dispatcherTeam.orders.push({id: dispatcherTeam.id, name: dispatcherTeam.name, 'assignedOrders': teamOrders.orders});
            dispatcherTeam.orders = teamOrders.orders;

            this.dispatcherTeams = result.data;
            console.log(this.dispatcherTeams);
            // this.toggleLoading = false;
          }
          // else{
          //   this.dispatcherTeams = result.data;
          //   this.toggleLoading = false;
          // }
        })
      })
    })
  }


  setActiveOrder(unassigned) {
    this.unasssignOrderToPost = unassigned;
    // console.log('member selected');
    console.log(this.unasssignOrderToPost)
  }

  setActiveTeamOrder(team) {
    this.teamOrderToPost = team;
    // console.log('member team selected');
    console.log('teamOrderToPost' + this.teamOrderToPost);
  }


  assignOrder(args) {

    setTimeout(() => {
      if (args[2].classList.value == '' && args[3] == null) {

        let orderObj = {
          teamId: this.teamOrderToPost.teamId,
          orderId: this.unasssignOrderToPost.id
        }
        console.log(orderObj)
        this.lookUp.assginOrder(orderObj).subscribe(res => {
          // this.toggleLoading = false;
        },
          err => {
            // console.log('fail');
          })
      }
      if (args[2].classList.value != '' && args[3] == null && args[1].classList.value != '') {

        let orderObj = {
          teamId: this.teamOrderToPost.teamId,
          orderId: this.unasssignOrderToPost.id
        }
        console.log(orderObj)
        this.lookUp.assginOrder(orderObj).subscribe(res => {
        },
          err => {
            // console.log('fail');
            // this.toggleLoading = false;
          })
      }
      else if (args[3] == null && args[1].classList.value == '') {
        let orderObj = {
          orderId: this.unasssignOrderToPost.id
        }
        this.lookUp.unAssginOrder(orderObj).subscribe(res => {
        },
          err => {
            // console.log('fail');
          })
      }
      // location.reload();
    }, 200);

  }

chooseColor(orderType, orderDate) {
		var style = {
			colorClass: "",
			orderTextColor: null
		};

		if (orderType == "MC Potential Orders") {
			style.colorClass = "white";
			style.orderTextColor = "black";
		}  else if (orderType == "Cash Compressor"){
			style.colorClass = "orange";
			style.orderTextColor = "black";
		}else if (orderType == "Cash call Order") {
			style.colorClass = "darkgreen";
			style.orderTextColor = "white";
		} else {
			if (orderType == "Preventive Maintenance Order" || orderType == "Enhancement Job Order" || orderType == "Contract Service Order") {
				var weekday = new Array(7);
				weekday[0] = "Sunday";
				weekday[1] = "Monday";
				weekday[2] = "Tuesday";
				weekday[3] = "Wednesday";
				weekday[4] = "Thursday";
				weekday[5] = "Friday";
        weekday[6] = "Saturday";
        var date  = new Date(orderDate); 
        var day = weekday[date.getDay()];
        console.log(day);
				switch (day) {
					case "Sunday":
						style.colorClass = "Pink";
						style.orderTextColor = "Black";
						break;
					case "Monday":
						style.colorClass = "lightgreen";
						break;
					case "Tuesday":
						style.colorClass = "lightblue";
						break;
					case "Wednesday":
						style.colorClass = "purple";
						style.orderTextColor = "White";
						break;
					case "Thursday":
						style.colorClass = "aqua";
						style.orderTextColor = "Black";
						break;
					case "Friday":
						style.colorClass = "lightgray";
						style.orderTextColor = "Black";
						break;
					case "Saturday":
						style.colorClass = "yellow";
						style.orderTextColor = "Black";
						break;
					default:
						style.colorClass = "black";
						style.orderTextColor = "white";
						break;
				}
			}else{
				var weekday = new Array(7);
				weekday[0] = "Sunday";
				weekday[1] = "Monday";
				weekday[2] = "Tuesday";
				weekday[3] = "Wednesday";
				weekday[4] = "Thursday";
				weekday[5] = "Friday";
				weekday[6] = "Saturday";
        var date  = new Date(orderDate); 
        var day = weekday[date.getDay()];
				switch (day) {
					case "Sunday":
						style.colorClass = "Pink";
						style.orderTextColor = "Black";
						break;
					case "Monday":
						style.colorClass = "lightgreen";
						break;
					case "Tuesday":
						style.colorClass = "lightblue";
						break;
					case "Wednesday":
						style.colorClass = "purple";
						style.orderTextColor = "White";
						break;
					case "Thursday":
						style.colorClass = "aqua";
						style.orderTextColor = "Black";
						break;
					case "Friday":
						style.colorClass = "lightgray";
						style.orderTextColor = "Black";
						break;
					case "Saturday":
						style.colorClass = "yellow";
						style.orderTextColor = "Black";
						break;
					default:
						style.colorClass = "black";
						style.orderTextColor = "white";
						break;
				}
			}
		}
		return style;
	}

}