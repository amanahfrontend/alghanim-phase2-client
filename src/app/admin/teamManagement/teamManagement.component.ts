import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { DragulaService } from 'ng2-dragula';
import { teamModelComponent } from '../../shared-module/shared/team/teamModel.component';
import { teamMemberModelComponent } from '../../shared-module/shared/teamMember/teamMember.component';


@Component({
  selector: 'app-teamManagement',
  templateUrl: './teamManagement.component.html',
  styleUrls: ['./teamManagement.component.css']
})


export class teamManagmentComponent implements OnInit {

  constructor(private lookUp: LookupService, private messageService: MessageService,
    private modalService: NgbModal, private utilities: UtilitiesService,private dragulaService: DragulaService) {
      
      // this.dragulaService.setOptions("disBag", {
      //   direction: 'horizontal',
      //   moves: (el, source, handle) => handle.className === "group-handle"
      // });
     
      // const unassignedbag: any = this.dragulaService.find('unassignedbag');
    // if (unassignedbag !== undefined) this.dragulaService.destroy('unassignedbag');
    // this.dragulaService.setOptions('unassignedbag', {
    //   revertOnSpill: true,
    //   accepts: function (el, target, source, sibling, sourceModel, targetModel, item) {
    //     // console.log(sibling)
    //     // console.log(el);
    //     // console.log(target);
    //     // console.log(source);
    //     // console.log(sourceModel);
    //     // console.log(targetModel);
    //     // console.log(item);

    //     return !el.contains(target);
    //   },
    // });

    this.dragulaService.drop.subscribe((value) => {
      console.log(value)
      if (value) {
        this.assignmembersAndTeams(value.slice(1));
      }
      else{
        this.dragulaService.setOptions('memberBag', {
          revertOnSpill: true
        });
      }
      // if (value[0]== 'teamBag') {
      // this.assignTeamToDispacher(value.slice(1));
      // }
    });
  }


  unassignedMembers: any = [];
  modalRef: any;
  dispatcherTeams: any = [];
  memberObj: any = {};
  dispatchers: any = []
  memberToPost: any;
  memberTeamToPost: any;
  teamToPost: any;
  dispatcherToPost: any;
  teams: any = [];
  dragEnabled: any;
  toggleLoading: boolean;

  ngOnInit() {
    this.getUnassignedMempers();
    this.getTeams();
  }

  assignmembersAndTeams(args) {
    // console.log('args' + args)
    setTimeout(() => {
      this.toggleLoading = true;
      
      // console.log(memberObj);

      if (args[2].classList.value == '' && args[1].classList.value != '') {
        let memberObj = {
          teamId: this.memberTeamToPost.id,
          memberId: this.memberToPost.id,
        };
        this.lookUp.assginMemberToTeam(memberObj).subscribe((res) => {
          // console.log(res);
          console.log('success');
          if (res.isSucceeded == true) {
            this.toggleLoading = false;
          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: res.status.message
            });
            this.toggleLoading = false;

            this.dragulaService.setOptions('memberBag', {
              revertOnSpill: true
            });
          }
        },
          err => {
            // console.log('fail');
            this.toggleLoading = false;
          })
      }
      else if (args[3] == null && args[1].classList.value != '') {
        let memberObj = {
          teamId: this.memberTeamToPost.id,
          memberId: this.memberToPost.id,
        };
        this.lookUp.assginMemberToTeam(memberObj).subscribe((res) => {
          // console.log(res);
          console.log('success');
          if (res.isSucceeded == true) {
            this.toggleLoading = false;
          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: res.status.message
            });
            this.toggleLoading = false;
                this.dragulaService.setOptions('memberBag', {
              revertOnSpill: true
            });
          }
        },
          err => {
            // console.log('fail');
            this.toggleLoading = false;
          })
      }
      else if (args[2].classList.value != '' && args[1].classList.value == '') {
        
      let unassignmemberObj = {
        memberId: this.memberToPost.id
      };
        this.lookUp.unAssginMember(unassignmemberObj).subscribe((res) => {
          console.log(res);
          if (res.isSucceeded == true) {
            this.toggleLoading = false;
          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: res.status.message
            });
            this.toggleLoading = false;
            this.dragulaService.setOptions('memberBag', {
              revertOnSpill: true
            });
          }
        },
          err => {
            console.log('fail');
            this.toggleLoading = false;
          })
      }
      // else if (args[1].classList.value == '' && args[2].classList.value == '' && args[3] == null) {
        else if (args[1].classList.value != '' && args[2].classList.value != '' && args[3].classList.value != '') {
        let teamObj = {
          teamId: this.teamToPost.id,
          dispatcherId: this.dispatcherToPost.dispatcherId,
        };
        this.lookUp.reassignTeamToDispatcher(teamObj).subscribe((res) => {
          // console.log(res);
          if (res.isSucceeded == true) {
            this.toggleLoading = false;
          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: res.status.message
            });
            this.toggleLoading = false;
            this.dragulaService.setOptions('memberBag', {
              revertOnSpill: true
            });
          }
        },
          err => {
            // console.log('fail');
            this.toggleLoading = false;
          })
      }
    }, 200);
  }

  assignTeamToDispacher(args) {
    // console.log('args' + args)
    setTimeout(() => {
      this.toggleLoading = true;
      let teamObj = {
        teamId: this.teamToPost.id,
        dispatcherId: this.dispatcherToPost.dispatcherId,
      };
      // console.log(teamObj);

      // if (args[0].classList.contains('teamMember-container')) {
      this.lookUp.reassignTeamToDispatcher(teamObj).subscribe((res) => {
        // console.log(res);
        if (res.isSucceeded == true) {
          this.toggleLoading = false;
        }
        else {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: res.status.message
          });
          this.toggleLoading = false;
        }
      },
        err => {
          // console.log('fail');
          this.toggleLoading = false;
        })
    }, 200);
  }

  setActiveMember(unassigned) {
    this.memberToPost = unassigned;
    // console.log('member selected');
    // console.log(this.memberToPost)
  }

  setActiveTeamMember(team) {
    this.memberTeamToPost = team;
    // console.log('member team selected');
    // console.log(this.memberTeamToPost);
  }

  setActiveTeam(team) {
    this.teamToPost = team;
    // console.log('team selected');
    // console.log(this.teamToPost);
  }

  setActiveDispatcher(dis) {
    this.dispatcherToPost = dis;
    // console.log('DISPATCHER selected');
    // console.log(this.dispatcherToPost);
  }

  activeMember(unassigned) {
    // console.log(unassigned);
  }

  getUnassignedMempers() {
    this.lookUp.getAllUnassignedMempers().subscribe(result => {
      this.unassignedMembers = result.data;
    }, error => {
      // console.log('error')
    })
  }

  getTeams() {
    this.lookUp.getTeams().subscribe(result => {
      this.dispatcherTeams = result.data;
      console.log(this.dispatcherTeams)
    }, error => {
      console.log('error')
    })
  }

  // getDispatchers() {
  //   this.lookUp.getAllDispatchers().subscribe(result => {
  //     this.dispatchers = result.data;
  //     console.log('dispatchers' + JSON.stringify(this.dispatchers))
  //     this.dispatchers.map(dis => {
  //       this.lookUp.getTeamByDispatcherById(dis.id).subscribe(result => {
  //         this.dispatcherTeams = result.data;
  //         console.log('dispatcherByIds' + JSON.stringify(this.dispatcherTeams))
  //         result.data.map(team => {
  //           if(dis.id == team.id){
  //             console.log('team.id' + team.id);
  //             console.log('dis.id' + dis.id)
  //           }
  //         })
  //       })
  //     })
  //   })
  // }

  // getDispatcherById(id) {
  //   this.lookUp.getTeamByDispatcherById(id).subscribe(result => {
  //     this.dispatcherTeams = result.data;
  //     console.log('dispatcherById' + JSON.stringify(this.dispatcherTeams))
  //   })
  // }

  openCreateTeamModel() {
    this.openModalTeam({}, 'create new team');
    this.modalRef.result.then((newTeam) => {
      this.getUnassignedMempers();
    })
  }
  openCreateTeamMemberModel() {
    this.openModalTeamMember({}, 'create new team member');
    this.modalRef.result.then((newTeam) => {
      this.getTeams();
    })
  }


  openModalTeam(data, header) {
    console.log('open Modal');
    this.modalRef = this.modalService.open(teamModelComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;
  }

  openModalTeamMember(data, header) {
    console.log('open Modal');
    this.modalRef = this.modalService.open(teamMemberModelComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;
  }

}