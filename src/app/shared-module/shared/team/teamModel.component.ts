import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { userService } from "../../../api-module/services/userService/userService.service";


@Component({
  selector: 'app-teamModel',
  templateUrl: './teamModel.component.html',
  encapsulation: ViewEncapsulation.None
})

export class teamModelComponent implements OnInit {

  @Input() header;
  @Input() data: any;
  statuses: any[];
  divisions: any = [];
  isSuccess: boolean = false;
  drivers: any = [];
  formans: any = [];
  superVisors: any = [];
  dispatchers: any = [];
  engineers: any = [];
  userObj: any = {};
  divisionObj: any = {};
  engObj: any = {};
  supervisorObj: any = {};
  dispatcherObj: any = {};
  statusObj: any = {};
  driverObj: any = {};
  formanObj: any = {};



  constructor(public activeModal: NgbActiveModal, private lookup: LookupService, private messageService: MessageService,
    private utilities: UtilitiesService, private userService: userService) {
  }

  ngOnInit() {
    console.log(this.header);
    console.log(this.data);

    this.getDivisions();
    this.getStatus()
  }

  getDivisions() {
    this.lookup.getDivision().subscribe(result => {
      this.divisions = result.data;
    }, error => {
      console.log('error');
    })
  }


  getEngineers(divisionObj) {
    this.lookup.getEngineerByDivision(divisionObj).subscribe(result => {
      this.engineers = result.data;
      console.log('engineers' + JSON.stringify(this.engineers))
    }, error => {
      console.log('error');
    })
  }

  getDrivers(divisionObj) {
    this.lookup.getDriverByDivision(divisionObj).subscribe(result => {
      this.drivers = result.data;
      console.log('drivers' + this.drivers)
    }, error => {
      console.log('error');
    })
  }
  
  getSuperVisor(divisionObj) {
    this.lookup.getSuperVisorByDivision(divisionObj).subscribe(result => {
      this.superVisors = result.data;
      console.log(this.superVisors)
    }, error => {
      console.log('error');
    })
  }

  getDispatchers(supervisorObj) {
    this.lookup.getDispatcherBySuper(supervisorObj).subscribe(result => {
      this.dispatchers = result.data;
      console.log(this.dispatchers)
    }, error => {
      console.log('error');
    })
  }

  getFormen(dispatcherObj) {
    this.lookup.getForemanByDispatcher(dispatcherObj).subscribe(result => {
      this.formans = result.data;
      console.log(this.dispatchers)
    }, error => {
      console.log('error');
    })
  }
  getStatus() {
    this.lookup.getAllAvailability().subscribe((data) => {
      this.statuses = data.data;
    }, error => {

    })
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }


  close(result) {
    console.log(result);
    result = {
      id: this.data.id,
      divisionId: result.divisionId,
      // description: result.description,
      // name: this.data.userName,
    }

    this.activeModal.close(result);
  }

  addTeam() {

    this.userObj.divisionId = this.divisionObj.id;
    this.userObj.divisionName = this.divisionObj.name;
    this.userObj.foremanId = this.formanObj.id;
    this.userObj.foremanName = this.formanObj.name;
    this.userObj.supervisorId = this.supervisorObj.id;
    this.userObj.supervisorName = this.supervisorObj.name;
    this.userObj.dispatcherId = this.dispatcherObj.id;
    this.userObj.dispatcherName = this.dispatcherObj.name;
    this.userObj.driverId = this.driverObj.id;
    this.userObj.driverName = this.driverObj.name;
    this.userObj.engineerId = this.engObj.id;
    this.userObj.engineerName = this.engObj.name;
    this.userObj.statusId = this.statusObj.id;
    this.userObj.statusName = this.statusObj.name;


    this.lookup.postTeam(this.userObj).subscribe(result => {
      if (result.isSucceeded == true) {
        this.messageService.add({
          severity: 'success',
          summary: 'Successful!',
          detail: "New value saved Successfully!."
        });
        this.activeModal.close()
      }
      else{
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: "Failed to Save due to network error"
        });
      }
      console.log(this.userObj);
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();

        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: "Failed to Save due to network error"
        });
      })
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
