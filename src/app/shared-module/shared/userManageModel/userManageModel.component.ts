import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { userService } from "../../../api-module/services/userService/userService.service";


@Component({
  selector: 'app-userManageModel',
  templateUrl: './userManageModel.component.html',
  encapsulation: ViewEncapsulation.None
})

export class userManageModelComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() role;
  @Input() data: any;
  statesSubscriptions: Subscription;
  states: any[];
  Days: any = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  dateToShowOnCal: any;
  defaultDate: any;
  startDate: Date;
  endDate: Date;
  divisions: any = [];
  isSuccess: boolean = false;
  userObj: any = { roleNames: [] };
  userData: any = {
    basic: { roleNames: [] },
    otherObj: {},
  }
  costCenters: any = [];
  superVisors: any = [];
  dispatchers: any = [];
  divisionObj: any = {};
  costCenter: any = {};
  supervisor: any = {};
  dispatcher: any = {};

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService, private messageService: MessageService,
    private utilities: UtilitiesService, private userService: userService) {
  }

  ngOnInit() {
    console.log(this.role);
    console.log(this.header);
    console.log(this.data);

    this.getDivisions();
    this.getCostCenter();
    this.getSuperVisor();
    this.getDispatchers();
    this.getRolesById();

  }

  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }

  getDivisions() {
    this.lookup.getDivision().subscribe(result => {
      this.divisions = result.data;
    }, error => {
      console.log('error');
    })
  }

  getCostCenter() {
    this.lookup.getCostCenter().subscribe(result => {
      this.costCenters = result.data;
    }, error => {
      console.log('error');
    })
  }

  getSuperVisor() {
    this.lookup.getAllSuperVisor().subscribe(result => {
      this.superVisors = result.data;
    }, error => {
      console.log('error');
    })
  }

  getDispatchers() {
    this.lookup.getAllDispatchers().subscribe(result => {
      this.dispatchers = result.data;
      console.log(this.dispatchers)
    }, error => {
      console.log('error');
    })
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }

  getRolesById() {
    if (this.data.id) {
      if (this.role == 'Admin') {
        this.lookup.getUserById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
        }, error => {
          console.log('error')
        })
      }
      if (this.role == 'Dispatcher') {
        this.lookup.getDispatcherById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
          this.divisionObj = {
            id: this.userData.basic.divisionId, name: this.userData.basic.divisionName
          }
          this.costCenter = {
            id: this.userData.basic.costCenterId, name: this.userData.basic.costCenterName
          }
          this.supervisor = {
            id: this.userData.basic.supervisorId, name: this.userData.basic.supervisorName
          }
        }, error => {
          console.log('error')
        })
      }
      if (this.role == 'Supervisor') {
        this.lookup.getSuperVisorById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
          this.divisionObj = {
            id: this.userData.basic.divisionId, name: this.userData.basic.divisionName
          }
          this.costCenter = {
            id: this.userData.basic.costCenterId, name: this.userData.basic.costCenterName
          }

        }, error => {
          console.log('error')
        })
      }
      if (this.role == 'Driver') {
        this.lookup.getDriverById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
          this.divisionObj = {
            id: this.userData.basic.divisionId, name: this.userData.basic.divisionName
          }
          this.costCenter = {
            id: this.userData.basic.costCenterId, name: this.userData.basic.costCenterName
          }

        }, error => {
          console.log('error')
        })
      }
      if (this.role == 'Engineer') {
        this.lookup.getEngineerById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
          this.divisionObj = {
            id: this.userData.basic.divisionId, name: this.userData.basic.divisionName
          }
          this.costCenter = {
            id: this.userData.basic.costCenterId, name: this.userData.basic.costCenterName
          }

        }, error => {
          console.log('error')
        })
      }
      if (this.role == 'Technician') {
        this.lookup.getTechnicianById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
          this.divisionObj = {
            id: this.userData.basic.divisionId, name: this.userData.basic.divisionName
          }
          this.costCenter = {
            id: this.userData.basic.costCenterId, name: this.userData.basic.costCenterName
          }
          this.supervisor = {
            id: this.userData.basic.supervisorId, name: this.userData.basic.supervisorName
          }
          this.dispatcher = {
            id: this.userData.basic.dispatcherId, name: this.userData.basic.dispatcherName
          }

        }, error => {
          console.log('error')
        })
      }
      if (this.role == 'Foreman') {
        this.lookup.getForemanById(this.data.id).subscribe(result => {
          this.userData.basic = result.data;
          this.divisionObj = {
            id: this.userData.basic.divisionId, name: this.userData.basic.divisionName
          }
          this.costCenter = {
            id: this.userData.basic.costCenterId, name: this.userData.basic.costCenterName
          }
          this.supervisor = {
            id: this.userData.basic.supervisorId, name: this.userData.basic.supervisorName
          }
          this.dispatcher = {
            id: this.userData.basic.dispatcherId, name: this.userData.basic.dispatcherName
          }
          // this.dispatcher = this.userData.basic.dispatcherName
        }, error => {
          console.log('error')
        })
      }
    }
    else {
      this.userData.basic = {};
    }
  }

  close(result) {
    console.log(result);
    result = {
      id: this.data.id,
      divisionId: result.divisionId,
      // description: result.description,
      // name: this.data.userName,
    }

    this.activeModal.close(result);
  }

  saveUser() {
    this.userData.basic.roleNames = [];
    this.userData.basic.roleNames.push(this.role);
    if (this.data.id) {
      if (this.role == 'Admin') {
        this.lookup.editUser(this.userData.basic).subscribe(result => {
          if (result.isSucceeded == true) {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully!."
            });
          }
          console.log(this.userData.dispatcherObj);
          this.activeModal.close()
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          })
      }
      if (this.role != 'Admin')  {
        this.lookup.editUser(this.userData.basic).subscribe(result => {
          if (result.isSucceeded == true) {
            this.isSuccess = true;
            this.userData.otherObj.id = this.data.id;
            this.userData.otherObj.userId = result.data.id;
            this.userData.otherObj.name = result.data.userName;
            this.userData.otherObj.divisionId = this.divisionObj.id;
            this.userData.otherObj.costCenterId = this.costCenter.id;
            this.userData.otherObj.supervisorId = this.supervisor.id;
            this.userData.otherObj.dispatcherId = this.dispatcher.id;
            this.userData.otherObj.divisionName = this.divisionObj.name;
            this.userData.otherObj.costCenterName = this.costCenter.name;
            this.userData.otherObj.supervisorName = this.supervisor.name;
            this.userData.otherObj.dispatcherName = this.dispatcher.name;


            if (this.role == 'Dispatcher') {
              this.lookup.editUserDispatcher(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.dispatcherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
            if (this.role == 'Supervisor') {
              this.lookup.editUserSuperVisor(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.otherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
            if (this.role == 'Manager') {
              this.lookup.editUserManager(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.otherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
            if (this.role == 'Engineer') {
              this.lookup.editUserEngineer(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.otherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
            if (this.role == 'Technician') {
              this.lookup.editUserTechnician(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.otherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
            if (this.role == 'Foreman') {
              this.lookup.editUserForeman(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.otherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
            if (this.role == 'Material Controller') {
              this.lookup.editUserControl(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                console.log(this.userData.otherObj);
                this.activeModal.close()
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                })
            }
          }

          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          }
        })
      }
    }
    else {
      this.userService.checkUserEmailExist(this.userData.basic.email).subscribe(result => {
        if (result.data == false) {
          this.userService.checkUserNameExist(this.userData.basic.userName).subscribe(result => {
            if (result.data == false) {
              this.userService.checkUserPhoneExist(this.userData.basic.phone1).subscribe(result => {
                if (result.data == false) {

                  if (this.role == 'Admin') {
                    this.lookup.postUser(this.userData.basic).subscribe(result => {
                      if (result.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      console.log(this.userData.otherObj);
                      this.activeModal.close()
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        })
                      })
                  }
                  if (this.role != 'Admin') {
                    this.lookup.postUser(this.userData.basic).subscribe(result => {
                      if (result.isSucceeded == true) {
                        this.isSuccess = true;

                        this.userData.otherObj.userId = result.data.id;
                        this.userData.otherObj.name = result.data.userName;
                        this.userData.otherObj.divisionId = this.divisionObj.id;
                        this.userData.otherObj.costCenterId = this.costCenter.id;
                        this.userData.otherObj.supervisorId = this.supervisor.id;
                        this.userData.otherObj.dispatcherId = this.dispatcher.id;
                        this.userData.otherObj.divisionName = this.divisionObj.name;
                        this.userData.otherObj.costCenterName = this.costCenter.name;
                        this.userData.otherObj.supervisorName = this.supervisor.name;
                        this.userData.otherObj.dispatcherName = this.dispatcher.name;

                        if (this.role == 'Dispatcher') {
                          this.lookup.postUserDispatcher(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                        if (this.role == 'Supervisor') {
                          this.lookup.postSuperVisor(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                        if (this.role == 'Manager') {
                          this.lookup.postManager(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                        if (this.role == 'Engineer') {
                          this.lookup.postEngineer(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                        if (this.role == 'Technician') {
                          this.lookup.postTechnician(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                        if (this.role == 'Foreman') {
                          this.lookup.postForeman(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                        if (this.role == 'Material Controller') {
                          this.lookup.postMatrialcontroller(this.userData.otherObj).subscribe((data) => {
                            if (data.isSucceeded == true) {
                              this.messageService.add({
                                severity: 'success',
                                summary: 'Successful!',
                                detail: "New value saved Successfully!."
                              });
                            }
                            console.log(this.userData.otherObj);
                            this.activeModal.close()
                          },
                            err => {
                              err.status == 401 && this.utilities.unauthrizedAction();

                              this.messageService.add({
                                severity: 'error',
                                summary: 'Failed!',
                                detail: "Failed to Save due to network error"
                              });
                            })
                        }
                      }
                      else {
                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        });
                      }
                    })
                  }
                }
                else {
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "this phone already exist"
                  });
                }
              })
            }
            else {
              this.messageService.add({
                severity: 'error',
                summary: 'Failed!',
                detail: "this user name already exist"
              });
            }
          })
        }
        else {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "this email already exist"
          });
        }
      })
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
