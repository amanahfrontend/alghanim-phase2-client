import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { MessageService } from 'primeng/components/common/messageservice';


@Component({
  selector: 'app-languageModel',
  templateUrl: './languageModel.component.html',
  styleUrls: ['./languageModel.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class languagesComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() type;
  @Input() data: any;
  statesSubscriptions: Subscription;
  allLanguages: any = [];
  langName: string = '';
  fK_SupportedLanguages_ID: any;
  langData: any = [];
  startDate: any = [Date];
  endDate: any = [Date];
  day: any = [];
  areaLangObj: any = {};
  toggleLoading: boolean;


  constructor(public activeModal: NgbActiveModal, private lookup: LookupService, private messageService: MessageService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.getAllLanguages();
    
  }

  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }

  ngAfterContentInit() {
    this.getAllAreaLanguages();
  }

  getAllLanguages() {
    this.lookup.getlanguages().subscribe(result => {
      this.allLanguages = result.data;
      console.log(this.allLanguages);
      // this.allLanguages.map(x=>{
      //   this.langData[x.id] = "";
      // });
    }, error => {
      console.log('error')
    })
  }

  getAllAreaLanguages() {
    console.log(this.data.id)
    this.toggleLoading = true;
    if (this.type == 'Area') {
      this.lookup.getAreaLangById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];

        // this.areaLangObj.filter(langs => {
        //   if (langs.name) {
        //     this.langData.push(langs);
        //     console.log('langDataNames' + ' ' + this.langData);
        //   }
        // })
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;
      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Attendance Status') {
      this.lookup.getAttendanceStatesById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Availabilty') {
      this.lookup.getAvailabilityById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Building Types') {
      this.lookup.getBuildingTypesById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Company Code') {
      this.lookup.getCompanyCodeById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Contract Type') {
      this.lookup.getContractTypeById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Cost Center') {
      this.lookup.getCostCenterById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Division') {
      this.lookup.getDivisionById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Governorates') {
      this.lookup.getGovernoratesById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }
    if (this.type == 'Shift') {
      this.lookup.getShiftById(this.data.id).subscribe(result => {
        this.areaLangObj = result.data;
        console.log("areaLangObj");
        console.log(this.areaLangObj);
        this.langData = [];
        this.initiateLanguages(result.data, this.data.id);
        this.toggleLoading = false;

      }, error => {
        console.log('error')
      })
    }

  }

  initiateLanguages(objs, objId) {
    for (let i = 0; i < this.allLanguages.length; i++) {
      if (this.type == 'Area') {
        this.langData.push({
          fK_Area_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Attendance Status') {
        this.langData.push({
          fK_AttendanceStates_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Availabilty') {
        this.langData.push({
          fK_Availability_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Building Types') {
        this.langData.push({
          fK_BuildingTypes_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Company Code') {
        this.langData.push({
          fK_CompanyCode_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Contract Type') {
        this.langData.push({
          fK_ContractTypes_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Cost Center') {
        this.langData.push({
          fK_CostCenter_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Division') {
        this.langData.push({
          fK_CostCenter_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Governorates') {
        this.langData.push({
          fK_Governorates_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Shift') {
        this.langData.push({
          fK_Shift_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          day: "",
        });
      }

    }

    if (objs && objs.length) {
      for (let i = 0; i < objs.length; i++) {
        // if(lang.fK_SupportedLanguages_ID == this.allLanguages[i].id){
        for (let j = 0; j < this.langData.length; j++) {
          if (this.langData[j].fK_SupportedLanguages_ID == objs[i].fK_SupportedLanguages_ID) {
            this.langData[j].id = objs[i].id;
            this.langData[j].name = objs[i].name;
            if (this.type == 'Shift') {
              this.langData[j].day = objs[i].day
            }
          }
        }
      }
    }

  }


  dataName(lang) {
    console.log(lang)
  }

  close() {
    console.log(this.type);
    let editedLungsObj = [];
    let addedLangsObj = [];
    for (let i = 0; i < this.langData.length; i++) {
      if (this.type == 'Area') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Area_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_Area_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Attendance Status') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_AttendanceStates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_AttendanceStates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Availabilty') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Availability_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_Availability_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Building Types') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_BuildingTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_BuildingTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Company Code') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_CompanyCode_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_CompanyCode_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Contract Type') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_ContractTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_ContractTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Cost Center') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_CostCenter_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_CostCenter_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Division') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Division_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_Division_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Governorates') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Governorates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name
          });
        }
        else {
          addedLangsObj.push({
            fK_Governorates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Shift') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Shift_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            day: this.langData[i].day,
            
          });
        }
        else {
          addedLangsObj.push({
            fK_Shift_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            day: this.langData[i].day
          });
        }
      }

    }

    if (this.type == 'Area') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editAreaLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postAreaLanguages(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Attendance Status') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editAttendanceStatesLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postAttendanceStatesLanguages(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Availabilty') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editAvailabilityLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postAvailabilityLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Building Types') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editBuildingTypesLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postBuildingTypesLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Company Code') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editCompanyCodeLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postCompanyCodeLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Contract Type') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editContractTypesLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postContractTypesLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Cost Center') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editCostCenterLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postCostCenterLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Division') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editDivisionLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postDivisionLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Governorates') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editGovernoratesLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postGovernoratesLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }
    if (this.type == 'Shift') {
      if (editedLungsObj && editedLungsObj.length) {
        this.lookup.editShiftLanguages(editedLungsObj).subscribe((data) => {
          console.log('result');
          console.log('langData' + JSON.stringify(editedLungsObj));
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      } else if (addedLangsObj && addedLangsObj.length) {
        this.lookup.postShiftLanguage(addedLangsObj).subscribe((data) => {
          console.log(data)
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: "New value saved Successfully! "
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to update due to network error"
            });
          })
      }
    }

    //   if(this.langData){
    //     if(this.type == 'Area'){
    //       for (let i = 0; i < this.langData.length; i++) {
    //         console.log('langData' + JSON.stringify(this.langData));
    //         // if(this.langData[i] == undefined){
    //         //   this.langData[i]= ''
    //         // }
    //         // else{
    //           result.push([{
    //             fK_Area_ID: this.data.id,
    //             fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
    //             id : this.langData[i].id,
    //             name: this.langData[i].name,
    //           }]);
    //       //   }
    //       }
    //       this.lookup.editAreaLanguages(result).subscribe((data) => {
    //         console.log(result)
    //         this.messageService.add({
    //           severity: 'success',
    //           summary: 'Successful!',
    //           detail: "New value saved Successfully! "
    //         });
    //       },
    //         err => {
    //           err.status == 401 && this.utilities.unauthrizedAction();
    //           this.messageService.add({
    //             severity: 'error',
    //             summary: 'Failed!',
    //             detail: "Failed to update due to network error"
    //           });
    //         })
    //     }
    //   }

    // else{

    //   if(this.type == 'Area'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_Area_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postAreaLanguages(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Attendance Status'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_AttendanceStates_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postAttendanceStatesLanguages(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Availabilty'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_Availability_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postAvailabilityLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Building Types'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_BuildingTypes_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postBuildingTypesLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Company Code'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_CompanyCode_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postCompanyCodeLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Contract Type'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_ContractTypes_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postContractTypesLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Cost Center'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_ContractTypes_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postContractTypesLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Division'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_Division_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postDivisionLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Governorates'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_Governorates_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i]
    //         });
    //       }
    //     }
    //     this.lookup.postGovernoratesLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //   if(this.type == 'Shift'){
    //     for (let i = 0; i <= this.allLanguages.length; i++) {
    //       if(this.langData[i] == undefined){
    //         this.langData[i]= ''
    //       }
    //       if(this.day[i] == undefined){
    //         this.day[i]= ''
    //       }
    //       else{
    //         result.push({
    //           fK_Shift_ID: this.data.id,
    //           fK_SupportedLanguages_ID: this.allLanguages[i-1].id,
    //           name: this.langData[i],
    //           day: this.day[i],
    //           fromTime: new Date(this.startDate[i]),
    //           toTime: new Date(this.endDate[i])
    //         });
    //       }
    //     }
    //     this.lookup.postShiftLanguage(result).subscribe((data) => {
    //       console.log(result)
    //       this.messageService.add({
    //         severity: 'success',
    //         summary: 'Successful!',
    //         detail: "New value saved Successfully! "
    //       });
    //     },
    //       err => {
    //         err.status == 401 && this.utilities.unauthrizedAction();
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update due to network error"
    //         });
    //       })
    //   }
    //  console.log(result);
    //   // else if (this.type == 'Shift') {
    //   //   result = {
    //   //     id: this.data.id,
    //   //     day: result.day,
    //   //     fromTime: result.fromTime,
    //   //     toTime: result.toTime
    //   //   }
    //   // }
    //   // else {
    //   //   result = {
    //   //     id: this.data.id,
    //   //     name: result.name,
    //   //   }
    //   // }
    // }
    this.activeModal.close();
  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
