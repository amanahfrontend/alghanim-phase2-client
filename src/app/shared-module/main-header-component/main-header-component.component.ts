import {Router} from '@angular/router';
import {AuthenticationServicesService} from './../../api-module/services/authentication/authentication-services.service';
import {Component, OnInit, EventEmitter, Output} from '@angular/core';
// import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {Subscription} from "rxjs/Subscription";
import {types} from "../../api-module/services/authentication/authentication-services.service"
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-main-header-component',
  templateUrl: './main-header-component.component.html',
  styleUrls: ['./main-header-component.component.css']
})

export class MainHeaderComponentComponent implements OnInit {
  isLoggedin: boolean = false;
  toggleNotification: boolean;
  newNotificationToggle: boolean;
  notificationBackgroundClass: any[];
  CurentUser: any = {};
  toggelingNotificationIconInterval: any;
  quotationSubscription: Subscription;
  callSubscription: Subscription;
  orderSubscription: Subscription;
  // logInSubscription: Subscription;
  notifications: any[] = [];
  toggelingNotificationIconClass: string[] = [];
  @Output() loggedOut = new EventEmitter();

  constructor(public authenticationService: AuthenticationServicesService, private router: Router, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.isLoggedin = this.authenticationService.isLoggedIn();
    this.CurentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
    // if (this.CurentUser) {
    this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    // let checkForLogInSubscription;
    // this.logInSubscription = this.authenticationService.isLoggedin.subscribe((value) => {

    // if (this.authenticationService.isLoggedin.value) {
    this.quotationSubscription = this.authenticationService.quotaionSignalR.subscribe((quotation) => {
        console.log('got the quotation');
        quotation.type = 'quotation';
        this.notifications.push(quotation);
        this.newNotificationToggle = true;
        this.toggelingNotificationIcon();
      },
      (err) => {
        console.log(err)
      });

    this.callSubscription = this.authenticationService.callSignalR.subscribe((call) => {
        console.log(call);
        console.log('got the call');
        call.type = 'call';
        this.notifications.push(call);
        this.newNotificationToggle = true;
        this.toggelingNotificationIcon();
      },
      (err) => {
        console.log(err)
      });

    // console.log('will subscribe to orders');
    this.orderSubscription = this.authenticationService.orderSignalR.subscribe((order) => {
        console.log('got the order');
        order.type = 'order';
        this.notifications.push(order);
        this.newNotificationToggle = true;
        this.toggelingNotificationIcon();
      },
      (err) => {
        console.log(err)
      });

    // } else {
    this.notifications = [];
    // this.orderSubscription && this.orderSubscription.unsubscribe();
    // this.callSubscription && this.callSubscription.unsubscribe();
    // this.quotationSubscription && this.quotationSubscription.unsubscribe();
    // }
    // });
  }

  routeToNotification(notificationData) {
    this.utilities.routingFromAndHaveSearch = true;
    this.utilities.currentSearch = {};
    if (notificationData.type == 'quotation') {
      this.utilities.currentSearch.searchText = notificationData.refNumber;
      this.utilities.currentSearch.searchType = 'quotation';
      this.utilities.routingFromAndHaveSearch = true;
      this.utilities.setSavedNotificationText(notificationData.refNumber);
      this.router.navigate(['/search/quotation'])
    } else if (notificationData.type == 'call') {
      this.utilities.currentSearch.searchText = notificationData.callerNumber;
      this.utilities.routingFromAndHaveSearch = true;
      this.utilities.setSavedNotificationText(notificationData.callerNumber);
      this.router.navigate(['/search/calls-history'])
    } else if (notificationData.type == 'order') {
      console.log('order');
      this.utilities.setSavedNotificationText(notificationData);
      // this.utilities.currentNotificationOrder = notificationData;
      this.router.navigate(['/dispatcher'])
    }
    this.toggleNotificationBody();
  }

  LogoutAll() {
    this.notifications = [];
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ['show-notification-icon'];
    this.stopIntervalNotificationIconToggeling();
    this.loggedOut.emit();
    this.router.navigate(['/login']);
  }

  toggleNotificationBody() {
    this.toggleNotification = !this.toggleNotification;
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ['show-notification-icon'];
    this.stopIntervalNotificationIconToggeling();
    if (this.notificationBackgroundClass.toString() == ['show-notification-transparent-background'].toString()) {
      this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    } else {
      this.notificationBackgroundClass = ['show-notification-transparent-background'];
    }
  }

  toggelingNotificationIcon() {
    this.toggelingNotificationIconInterval = setInterval(() => {
      console.log(this.newNotificationToggle);
      if (this.newNotificationToggle) {
        if (this.toggelingNotificationIconClass.toString() == ['show-notification-icon'].toString()) {
          this.toggelingNotificationIconClass = ['hide-notification-icon'];
        } else {
          this.toggelingNotificationIconClass = ['show-notification-icon'];
        }
      } else {
        this.toggelingNotificationIconClass = ['show-notification-icon'];
      }
      // this.toggelingNotificationIcon(this.newNotificationToggle);
    }, 1000)
  }

  stopIntervalNotificationIconToggeling() {
    clearInterval(this.toggelingNotificationIconInterval);
  }

}
