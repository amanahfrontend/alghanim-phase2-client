import { Component, OnInit, OnDestroy, OnChanges, AfterViewInit } from '@angular/core';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { Subscription } from "rxjs";
import { AuthenticationServicesService } from "../../../api-module/services/authentication/authentication-services.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as signalR from "@aspnet/signalr";
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-printOrder',
    templateUrl: './printOrder.component.html',
    styleUrls: ['./printOrder.component.css']
})

export class printOrderComponent implements OnInit {

    toggleLoading: boolean;
    orders: any[];
    filteredOrders: any[] = [];
    orderMap: any[];
    tecs: any[] = [];
    cornerMessage: any[];
    selectedOrders: any[] = [];
    dispatcherOrdersSubscription: Subscription;
    tecsByDisSubscription: Subscription;
    orderStatusObservable: Subscription;
    allVehiclesSubscription: Subscription;
    isOrderSelected: any;
    orderToPost: any;
    tecToPost: any;
    toggleMap: boolean;
    isBulkAssign: boolean;
    toggleOverflowX: string[];
    display: boolean;
    allVehicles: any[] = [];
    availabilityButtons: any[] = [];
    filterStatusClass: string[] = [];
    todayOrdersStatus: boolean;
    vehiclesStatus: boolean;
    notificationSubscription: Subscription;
    lang = localStorage.getItem('lang');
    seletetdTecs: any = [];
    allTecs: any = [];


    constructor(private lookup: LookupService, private utilities: UtilitiesService,
        private authService: AuthenticationServicesService, private modelService: NgbModal,
        private translate: TranslateService) {
        translate.setDefaultLang(this.lang);

    }

    ngOnInit() {
        this.getDispatcherOrders();
        this.getTecsByDis();
        this.getAllTecsByDisDropdown();
        this.display = true;
        this.todayOrdersStatus = true;
        this.vehiclesStatus = true;
        this.utilities.toggleFullWidth = true;
        this.filterStatusClass = ['zero-filter-width'];

    }

    orderTransfered(id) {
        console.log(id);
        this.orders = this.orders.filter((order) => {
            return id != order.id;
        });
        console.log(id);
        this.tecs.map((tec) => {
            tec.orders = tec.orders.filter((order) => {
                return id != order.id;
            })
        });
    }

    applyFilterMap(filteredOrders) {
        this.orderMap = filteredOrders;
        console.log('orderMap' + this.orderMap)
        this.orderMap.map((order) => {
            if (order.orderStatus) {
                order.statusUrl = 'assets/Images/' + order.orderStatus.id + '.png';
            }
        });
        this.todayOrdersStatus = false;
    }

    private _filterOrdersListBoard(list, terms) {
        console.log(terms);
        let termsCount = 0
            , filterOrders = [];

        for (let property in terms) {
            if (terms.hasOwnProperty(property)) {
                termsCount++;
            }
        }

        list.map((order) => {
            let existedTermsCount = 0;
            console.log(order);
            if (terms.orderNo == order.code) {
                existedTermsCount++;
            }
            if (terms.status) {
                terms.status.map((state) => {
                    if (state.id == order.orderStatus.id) {
                        existedTermsCount++;
                    }
                })
            }
            if (terms.problems) {
                terms.problems.map((problem) => {
                    if (problem.id == order.orderProblem.id) {
                        existedTermsCount++;
                    }
                })
            }
            console.log(new Date(terms.startDateFrom) < new Date(order.createdDate));
            if (terms.startDateFrom) {
                if (new Date(terms.startDateFrom) <= new Date(order.createdDate)) {
                    existedTermsCount++;
                }
            }
            if (terms.startDateTo) {
                if (new Date(terms.startDateTo) >= new Date(order.createdDate)) {
                    existedTermsCount++;
                }
            }
            if (existedTermsCount == termsCount) {
                filterOrders.push(order);
            }
        });
        return filterOrders;
    }

    applyFilterBoard(terms) {
        this.tecs.map((tec) => {
            tec.filterOrders = this._filterOrdersListBoard(tec.orders, terms);
        });
        this.filteredOrders = this._filterOrdersListBoard(this.orders, terms)
    }

    resetBoardFilter() {
        this.filteredOrders = this.orders.slice();
        this.tecs.map((tec) => {
            tec.filterOrders = tec.orders.slice();
        })
    }

    toggleAvailabilityClass(tec) {
        if (!tec.isAvailable) {
            return ['not-available']
        }
    }

    postTecAvailability(tec) {
        console.log(tec);
        // console.log(e);
        // e.preventDefault();
        if (!tec.orders.length) {
            let availability = {
                id: tec.id,
                isAvailable: tec.isAvailable
            };
            console.log(availability);
            this.lookup.postTecAvailability(availability)
                .subscribe(() => {
                    console.log('success')
                },
                    err => {

                    })
        } else {
            tec.isAvailable = true;
            // e.checked = true;
            // console.log(e);
        }
    }

    checkTecOrders(tec) {
        return !!tec.filterOrders.length;
    }

    private _moveOrdersBetweenSections(orderId, tecId) {
        let selectedOrder;
        this.orders.map((order) => {
            if (order.id == orderId) {
                selectedOrder = order;
            }
        });
        this.tecs.map((tec) => {
            return tec.filterOrders.map((order) => {
                if (order.id == orderId) {
                    selectedOrder = order;
                }
            });
        });

        console.log(selectedOrder);

        if (tecId) {
            this.tecs.map((tec) => {
                if (tec.id == tecId) {
                    tec.filterOrders.splice(tec.filterOrders.indexOf(selectedOrder), 1)
                }
            })
        } else {
            this.orders.splice(this.orders.indexOf(selectedOrder), 1)
        }
    }

    postOrderByTec(val) {
        setTimeout(() => {
            let orderByTec = {
                fK_technican_id: this.tecToPost.id,
                fK_order_id: this.orderToPost.id,
                // isTransfer: false
            };
            console.log(this.orderToPost);
            console.log(orderByTec);
            // console.log(val);
            if (val[1].classList.contains('single-tec-overflow')) {
                this.lookup.postOrderToTec(orderByTec).subscribe((res) => {
                    console.log(res);
                    console.log('success');
                    this.getDispatcherOrders();
                    this.getTecsByDis();
                },
                    err => {
                        console.log('fail');
                    })
            }
            else if (val[1].classList.contains('orders-overflow-container')) {
                this.lookup.postUnAssignedOrder(this.orderToPost.id).subscribe((res) => {
                    console.log(res);
                    // this.orderToPost.fK_Technican_Id = null;
                    console.log('success');
                    this.getDispatcherOrders();
                    this.getTecsByDis();
                },
                    err => {
                        console.log('fail');
                    })
            }
        }, 200);

    }

    getDispatcherOrders() {
        this.toggleLoading = true;
        let dispatcherId = this.authService.CurrentUser().id;
        this.dispatcherOrdersSubscription = this.lookup.getDispatcherOrders(dispatcherId).subscribe((dispatcher) => {
            this.orders = dispatcher.orders;
            console.log("this.orders");
            console.log(this.orders);
            this.orders.map((order) => {
                order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
            });
            this.filteredOrders = this.orders.slice();
            // this.orderMap = this.orders.slice();
            //console.log(this.orders);
            this.toggleLoading = false;
        },
            err => {
                this.toggleLoading = false;
            })
    }

    getAllTecsByDisDropdown() {
        let dispatcherId = this.authService.CurrentUser().id;
        this.tecsByDisSubscription = this.lookup.getTecsByDis(dispatcherId).subscribe((allTecs) => {
            this.allTecs = allTecs;
            console.log(JSON.stringify('tecs', this.allTecs))
        },
            err => {
            })
    }

    getTecsByDis() {
        let dispatcherId = this.authService.CurrentUser().id;
        this.tecsByDisSubscription = this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
            this.tecs = tecs;
            console.log(JSON.stringify('tecs', this.tecs))
            this.tecs.map((tec) => {
                tec.filterOrders = tec.orders.slice();
                console.log('filterOrders' + tec.filterOrders)
                tec.orders.map((order) => {
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                })
            });
            console.log(tecs);
        },
            err => {
            })
    }

    changeTecs(list) {
        if (list) {
            this.tecs = list;
            console.log(this.tecs)
            this.tecs.map((tec) => {
                tec.filterOrders = tec.orders.slice();
            })
        }
        //   this.tecs.map((tec) => {
        //     tec.filterOrders = list;
        //   })
    }

    print() {
        let printContents, popupWin;
        printContents = document.getElementById('orderPrint').innerHTML;
        popupWin = window.open('Orders', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open("");
        popupWin.document.write(`
    <html>
      <head>
        
        <style>
        //........Customized style.......
        * {
            box-sizing: border-box !important;
        }
        html body .uk-card {
            padding-top: 5px !important;
            padding-bottom: 25px !important;
            margin-top: 15px !important;
            margin-bottom: 15px !important;
            border: 1px solid #f0f0f0 !important;
            padding-right: 2% !important;
          }
          html body .uk-card.uk-card-default {
            box-shadow: 1px 3px 4px 0 rgba(0, 0, 0, 0.15) !important;
          }
          .uk-card-default {
            background: #fff !important;
            color: #666 !important;
            box-shadow: 0 5px 15px rgba(0,0,0,.08) !important;
        }
        .uk-card {
            position: relative;
            box-sizing: border-box !important;
            transition: box-shadow .1s ease-in-out !important;
        }
        .uk-card-header{
            padding: 0px 15px !important;
        }
        .uk-card-default .uk-card-header {
            border-bottom: 1px solid #e5e5e5 !important;
        }
        .orderTitle{
            color: brown;
        }
        .uk-card-body{
            padding: 0px 15px !important;
        }
        .col-md-6 {
            width: 50% !important;
        }
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
        );
        popupWin.print();
        popupWin.close();
    }


}
