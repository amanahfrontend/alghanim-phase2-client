import {SharedModuleModule} from './shared-module/shared-module.module';
// import {SignalRService} from './shared-module/signal-r.service';
import {RoutingModule, RoutingComponents} from './router.module';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpModule, BaseRequestOptions} from '@angular/http';
import { NgDragDropModule } from 'ng-drag-drop';
import { pagesItemSharedService } from './api-module/pagesItemsShared/pagesItemsShared.service'

import {MessageService} from 'primeng/components/common/messageservice';
import {UtilitiesService} from "./api-module/services/utilities/utilities.service";

let token = '';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthenticationServicesService} from "./api-module/services/authentication/authentication-services.service";

import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';
import { DispatcherPipe } from './admin/dispatcher.pipe';


@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    DispatcherPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    DragulaModule,
    NgDragDropModule.forRoot(),
    SharedModuleModule.forRoot(),
    // SignalRModule.forRoot(createConfig)
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    BaseRequestOptions, UtilitiesService, MessageService, AuthenticationServicesService,pagesItemSharedService],
  bootstrap: [AppComponent],

})
export class AppModule {
}
