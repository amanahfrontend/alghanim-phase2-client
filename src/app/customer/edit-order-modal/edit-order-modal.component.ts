import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Message} from "primeng/components/common/message";

@Component({
  selector: 'app-edit-order-modal',
  templateUrl: './edit-order-modal.component.html',
  styleUrls: ['./edit-order-modal.component.css']
})
export class EditOrderModalComponent implements OnInit {
  @Input() data: any;
  @Input() notificationOrder: boolean;
  modalMessage: Message[] = [];

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngOnInit() {
  }

  updateOrder(order) {
    //console.log(order);
    //console.log(this.data);
    order.id = this.data.id;
    order.code = this.data.code;
    order.fK_Contract_Id = this.data.fK_Contract_Id;
    order.fK_Technican_Id = this.data.fK_Technican_Id;
    order.fK_Dispatcher_Id = this.data.fK_Dispatcher_Id;
    order.fK_Location_Id = this.data.fK_Location_Id;
    order.fK_Customer_Id = this.data.fK_Customer_Id;
    order.quotationRefNo = this.data.quotationRefNo;
    order.price = this.data.price;
    order.fK_OrderStatus_Id = this.data.fK_OrderStatus_Id;
    order.preferedVisitTime = this.data.preferedVisitTime;
    this.lookup.updateOrder(order).subscribe(() => {
        console.log(order);
        this.modalMessage.push({
          severity: "success",
          summary: "Order Saved Successfully",
          detail: "Order Edited and saved successfully!"
        });
        this.activeModal.close()
      },
      err => {
        this.modalMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to save order due to network error!"
        });

      })
  }

}
